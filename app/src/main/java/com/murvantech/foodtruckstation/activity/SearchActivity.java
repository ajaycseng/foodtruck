package com.murvantech.foodtruckstation.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.murvantech.foodtruckstation.R;
import com.murvantech.foodtruckstation.model.TruckData;
import com.murvantech.foodtruckstation.networkcall.IResult;
import com.murvantech.foodtruckstation.networkcall.URLS;
import com.murvantech.foodtruckstation.networkcall.VolleyService;
import com.murvantech.foodtruckstation.utils.Constant;
import com.murvantech.foodtruckstation.utils.CustomAutoCompleteTextView;
import com.murvantech.foodtruckstation.utils.GPSTracker;
import com.murvantech.foodtruckstation.utils.HideKeyboard;
import com.murvantech.foodtruckstation.utils.PlaceJSONParser;
import com.murvantech.foodtruckstation.utils.Preference;
import com.murvantech.foodtruckstation.utils.Utils;
import com.squareup.picasso.Picasso;
import com.testfairy.TestFairy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class SearchActivity extends FragmentActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, OnMapReadyCallback {

    private Gson gson = new Gson();
    private IResult resultCallback;
    private VolleyService mVolleyService;
    ArrayList<TruckData> castleList = new ArrayList<>();
    CastleListAdapter castleAdapter = new CastleListAdapter();

    // SwipeRefreshLayout swipeRefreshLayout;
    LinearLayoutManager layoutManager;

    RecyclerView rvCastlelist;
    private Context context;
    private String categoryID = "1";


    private String userID = "";
    ImageView ivBack;
    TextView tvRetry;

    private static final int NOT_SELECTED = 0;
    private int selectedPos = NOT_SELECTED;
    private GPSTracker tracker;

    private double lat = 0.0;
    private double lng = 0.0;
    private String truckID = "";

    private ImageView ivSearch;
    private String searchKeyword = "";

    private GPSTracker gps;


    ArrayList<HashMap<String, String>> al_data = new ArrayList<HashMap<String, String>>();
    CustomAutoCompleteTextView etSearch;
    private ParserAutoTask parserAutoTask;
    private String placeID;
    private PlacesTask placesTask;
    String strplace = "";
    private GoogleMap mMap;


    private RelativeLayout rlListView, rlMapView;
    private ImageView ivList, ivMap;
    private Hashtable<String, String> markers = new Hashtable<>();
    private ArrayList<Marker> markerList;

    private CircleImageView imageView;
    private View marker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TestFairy.begin(this, Constant.TEST_FAIRY_TOKEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        new HideKeyboard().setupUI(findViewById(R.id.ll), this);
        context = this;


        marker = ((LayoutInflater) SearchActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
        imageView = (CircleImageView) marker.findViewById(R.id.riImg);

        ivList = findViewById(R.id.ivList);
        ivMap = findViewById(R.id.ivMap);
        rlMapView = findViewById(R.id.rlMapView);
        rlListView = findViewById(R.id.rlListView);
        userID = Preference.getInstance(context).getString(Constant.USER_ID);
        markerList = new ArrayList<Marker>();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        // swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        ivBack = findViewById(R.id.ivBack);
        // swipeRefreshLayout.setOnRefreshListener(this);

        tvRetry = findViewById(R.id.tvRetry);
        rvCastlelist = findViewById(R.id.rvCastlelist);
        layoutManager = new LinearLayoutManager(context);
        rvCastlelist.setLayoutManager(layoutManager);
        rvCastlelist.setAdapter(castleAdapter);
        rvCastlelist.setVisibility(View.VISIBLE);
        etSearch = findViewById(R.id.etSearch);
        ivSearch = findViewById(R.id.ivSearch);
        ivSearch.setOnClickListener(this);

        ivBack.setOnClickListener(this);
        ivList.setOnClickListener(this);
        ivMap.setOnClickListener(this);
        castleAdapter.notifyDataSetChanged();


        etSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                InputMethodManager inputManager =
                        (InputMethodManager) SearchActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(
                        SearchActivity.this.getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);


                searchKeyword = etSearch.getText().toString().trim();


                try {

                    LatLng latLng = Utils.GetLatLongFromAddress(SearchActivity.this, searchKeyword);


                    if (latLng != null) {
                        lat = latLng.latitude;
                        lng = latLng.longitude;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                getTruckList();


            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
             /*   if (s.length() == 0) {
                    ivCrossLocation.setVisibility(View.INVISIBLE);
                } else {
                    ivCrossLocation.setVisibility(View.VISIBLE);


                }*/

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                al_data.clear();
                strplace = s.toString();
                placesTask = new PlacesTask();
                placesTask.execute(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                //loc = "pickup";


            }
        });


    }


    @Override
    public void onMapReady(GoogleMap Map) {
        mMap = Map;


     /*   mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                InputMethodManager inputManager =
                        (InputMethodManager) HomeActivty.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(
                        HomeActivty.this.getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });

*/
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {


                String currentString = marker.getSnippet();
                String[] separated = currentString.split("--");
                String TruckID = separated[0];
                String isEmployee = separated[1];


                startActivity(new Intent(SearchActivity.this, TruckDetailActivity.class).putExtra("ID", TruckID));


                return false;
            }
        });


    }


    private void showOnMap() {


      /*  runOnUiThread(new Runnable() {
            @Override
            public void run() {*/

        mMap.clear();
        for (final TruckData shoplist : castleList) {

            LatLng latLng;
            try {
                latLng = new LatLng(Double.valueOf(shoplist.getTruck_lat()), Double.valueOf(shoplist.getTruck_long()));
            } catch (NumberFormatException e) {
                latLng = new LatLng(0.0, 0.0);
            } catch (Exception e) {
                latLng = new LatLng(0.0, 0.0);
            }


            String imgUrl = "";
            if (!shoplist.getTruck_image().equals("")) {
                imgUrl = shoplist.getTruck_image();

            }


            final LatLng finalLatLng = latLng;
            Glide.with(getApplicationContext())
                    .load(imgUrl).asBitmap().fitCenter()
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                            String id = shoplist.getId();
                            Marker mark = mMap.addMarker(new MarkerOptions().position(finalLatLng).snippet(id + "--" + shoplist.getUser_id())
                                    .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(marker, bitmap))));


                            markers.put(mark.getId() + "", shoplist.getId());


                            markerList.add(mark);
                        }
                    });


        }
        LatLng latLng = new LatLng(Double.parseDouble(castleList.get(0).getTruck_lat()), Double.parseDouble(castleList.get(0).getTruck_long()));
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 20);
        mMap.animateCamera(cameraUpdate);

    }


    private Bitmap getMarkerBitmapFromView(View view, Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = view.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        view.draw(canvas);
        return returnedBitmap;

    }


    @Override
    public void onRefresh() {
        castleList.clear();
        castleAdapter.notifyDataSetChanged();
        //   swipeRefreshLayout.setRefreshing(false);
        //getTruckList();
    }


    private void getTruckList() {
        initCallbackDL();
        mVolleyService = new VolleyService(resultCallback, context);
        mVolleyService.postStringRequest(context, "POST", URLS.SEARCH, getParamsDL(), SplashActivity.TOKEN);
    }

    private Map<String, String> getParamsDL() {
        JSONObject jsonStr = new JSONObject();


        try {
            // jsonStr.put("keyword", searchKeyword);

            jsonStr.put("location", searchKeyword);
            jsonStr.put("lat", "" + lat);
            jsonStr.put("lng", "" + lng);

            // jsonStr.put("lat", "" + lat);
            // jsonStr.put("lng", "" + lng);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("jsonData", jsonStr.toString());
        return map;
    }

    private void initCallbackDL() {
        resultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("response_status");
                    String msg = jsonObject.getString("response_msg");

                    if (status.equalsIgnoreCase("success")) {

                        tvRetry.setVisibility(View.GONE);
                        rvCastlelist.setVisibility(View.
                                VISIBLE);
                        castleList.clear();
                        JSONObject jsonObj = jsonObject.getJSONObject("response_data");
                        JSONArray jsonArray = jsonObj.getJSONArray("trucks_list");

                        if (jsonArray.length() == 0) {
                            //tvRetry.setVisibility(View.VISIBLE);
                            //swipeRefreshLayout.setRefreshing(false);
                            castleAdapter.notifyDataSetChanged();
                            tvRetry.setVisibility(View.VISIBLE);
                            rvCastlelist.setVisibility(View.
                                    GONE);


                        } else {
                            // tvRetry.setVisibility(View.GONE);

                            for (int i = 0; i < jsonArray.length(); i++) {

                                TruckData order;
                                order = gson.fromJson(jsonArray.getJSONObject(i).toString(), TruckData.class);
                                castleList.add(order);

                            }

                            //showToast("" + al_data.size());
                            //  swipeRefreshLayout.setRefreshing(false);
                            castleAdapter.notifyDataSetChanged();
                        }


                        showOnMap();
                    } else {
                        tvRetry.setVisibility(View.VISIBLE);
                        rvCastlelist.setVisibility(View.
                                GONE);

                        Utils.dialog(context, msg);

                    }

                } catch (JSONException e) {
                    tvRetry.setVisibility(View.VISIBLE);
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Utils.getErrors(context, error.toString());
                tvRetry.setVisibility(View.VISIBLE);
            }
        };
    }


    private class CastleListAdapter extends RecyclerView.Adapter<CastleListAdapter.ViewHolder> {
        @Override
        public CastleListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_truck_list, parent, false);
            return new CastleListAdapter.ViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(CastleListAdapter.ViewHolder holder, final int position) {
            final TruckData userData = castleList.get(position);


// set the drawable as progress drawable

            holder.tvDescription.setText(userData.getDescription());
            holder.tvName.setText(userData.getName());
            holder.tvContact.setText(userData.getPhone());


            holder.tvContact.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("MissingPermission")
                @Override
                public void onClick(View v) {


                    if (!userData.getPhone().equals("")) {

                        try {


                            Intent sIntent = new Intent(Intent.ACTION_CALL, Uri

                                    .parse("tel:" + userData.getPhone()));

                            sIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


                            startActivity(sIntent);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            try {


                String userImg = userData.getTruck_image();


                if (userImg != null && !userImg.isEmpty()) {

                    if (userImg.contains("no_image")) {
                        Picasso.with(context).load(R.drawable.logo).error(R.drawable.logo).fit().into(holder.ivUser);

                    } else {

                        Picasso.with(context).load(userImg).error(R.drawable.logo).resize(200, 200).into(holder.ivUser);
                    }
                } else {
                    Picasso.with(context).load(R.drawable.logo).error(R.drawable.logo).fit().into(holder.ivUser);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.ll_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    startActivity(new Intent(context, TruckDetailActivity.class)
                            .putExtra("ID", userData.getId()));
                   /* startActivity(new Intent(context, CastleDetailActivity.class)
                            .putExtra("DATA", restroArrayList));
*/
                }
            });


        }

        @Override
        public int getItemCount() {
            return castleList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvName, tvDescription, tvContact;
            CircleImageView ivUser;
            LinearLayout ll_main;


            public ViewHolder(View itemView) {
                super(itemView);
                tvDescription = (TextView) itemView.findViewById(R.id.tvAddress);
                tvName = (TextView) itemView.findViewById(R.id.tvName);
                tvContact = (TextView) itemView.findViewById(R.id.tvContact);

                ll_main = (LinearLayout) itemView.findViewById(R.id.ll);
                ivUser = (CircleImageView) itemView.findViewById(R.id.ivUser);

            }
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.ivMap:
                rlMapView.setVisibility(View.VISIBLE);
                rlListView.setVisibility(View.GONE);

                break;

            case R.id.ivList:

                rlMapView.setVisibility(View.GONE);
                rlListView.setVisibility(View.VISIBLE);

                break;

            case R.id.ivBack:

                finish();
                break;

            case R.id.ivSearch:

                searchKeyword = etSearch.getText().toString().trim();
                if (!searchKeyword.equals("")) {
                    getTruckList();
                }


                break;


        }
    }

    private void getLocation() {
        tracker = new GPSTracker(SearchActivity.this);
        if (tracker.canGetLocation()) {

            //  lat = 35.8533935;
            // lng = -78.7024126;
            lat = tracker.getLatitude();
            lng = tracker.getLongitude();
            if (lat != 0.0) {

                updateLocationWS();
            } else {
                Utils.showDialog(SearchActivity.this, "Location not found. Please enable your GPS");
            }
        } else {

        }
    }


    private void updateLocationWS() {

        initVolleyCallbackUpdate();
        mVolleyService = new VolleyService(resultCallback, context);
        mVolleyService.postStringRequest(context, "GET", URLS.UPDATE_LOCATION, getParamsLocatioin(), SplashActivity.TOKEN);
    }

    private Map<String, String> getParamsLocatioin() {

        JSONObject dataa = new JSONObject();
        try {
            dataa.putOpt("user_id", Preference.getInstance(SearchActivity.this).getString(Constant.USER_ID));
            dataa.putOpt("lat", "" + lat);
            dataa.putOpt("lng", "" + lng);
            dataa.putOpt("truck_id", "" + truckID);
            //  dataa.putOpt("Language", Constant.LANGUAGE);
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Map<String, String> params = new HashMap<String, String>();
        // params.put("api_key", Constant.API_KEY);
        params.put("jsonData", dataa.toString());
        return params;
    }

    void initVolleyCallbackUpdate() {
        resultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, String response) {
                Log.d(Constant.TAG, " Success " + requestType);
                try {
                    JSONObject data = new JSONObject(response);

                    JSONObject responseData;


                    if (data.getString(Constant.RESPONSE_STATUS).equalsIgnoreCase(Constant.SUCCESS)) {
                        /// showToast(data.getString("response_msg"));

                        getTruckList();
                        Utils.showDialog(SearchActivity.this, data.getString("response_msg"));


                    } else {
                        showToast(data.getString("response_msg"));

                    }
                } catch (JSONException e) {
                    showToast(e.getLocalizedMessage());
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(Constant.TAG, " Error " + requestType);
                showToast(error.toString());

            }
        };
    }


    private void showToast(String localizedMessage) {
        //new MyCustomeToast().showToast(this, localizedMessage);
        Utils.showDialog(context, localizedMessage);
    }


    private class PlacesTask extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            /*
             * gpsTracker=new GPSTracker(AddEventActivity.this);
             * if(gpsTracker.canGetLocation()) { lat=gpsTracker.getLatitude();
             * lang=gpsTracker.getLongitude(); }
             */
            String data = "";

            // Obtain browser key from https://code.google.com/apis/console
            String key = "key=" + "AIzaSyAxrFTAeywjHoEVG9R7UKuCpxv9uM7osYw";

            String input = "";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            // place type to be searched
            String types = "types=geocode";

            // Sensor enabled
            String sensor = "sensor=true";
            String
                    location = "location=" + String.valueOf(lat) + "," + String.valueOf(lng);
            // Building the parameters to the web service
/*
            String parameters = input + "&" + sensor + "&" + key + "&"
                    + "location=0.000000,0.000000" + "&" + "radius=100.000000";
*/
            String parameters = input + "&" + sensor + "&" + key + "&"
                    + "location=" + location + "&radius=100.000000&language=en&types=";//(cities)

            // Output format
            String output = "json";

            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/"
                    + output + "?" + parameters;
            // System.out.println("url = " + url);
            try {
                // Fetching the data from we service
                data = downloadUrl(url);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Creating ParserTask
            parserAutoTask = new ParserAutoTask();

            // Starting Parsing the JSON string returned by Web Service
            parserAutoTask.execute(result);
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception in url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }

        return data;

    }

    private class ParserAutoTask extends
            AsyncTask<String, Integer, List<HashMap<String, String>>> {

        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(
                String... jsonData) {

            List<HashMap<String, String>> places = null;

            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);

                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {

            SimpleAdapter adapter;
            // data = new JSONArray(result);


              /*  List<HashMap<String, String>> uniqueList = new ArrayList();
                for (HashMap<String, String> prod : result) {
                    HashMap<String, String> foundProd = null;
                    for (HashMap<String, String> uniqueProd : uniqueList) {
                        if (uniqueProd.get("city").equals(prod.get("city"))) {
                            foundProd = uniqueProd;
                            break;
                        }
                    }
                    if (foundProd == null) {
                        uniqueList.add(prod);


                    } else {

                        // Do something if the product already existed (maybe update qty)
                    }
                }

                al_data.clear();
                al_data.addAll(uniqueList);


                String[] from = new String[]{"city"};
*/

            try {

                al_data = new ArrayList<>();
                al_data.addAll(result);

                String[] from = new String[]{"description"};

                int[] to = new int[]{R.id.staus_name};

                TextView textView = findViewById(R.id.staus_name);
                adapter = new SimpleAdapter(SearchActivity.this,
                        al_data, R.layout.row_city, from, to);

                // Setting the adapter
                // etCityTemp.setAdapter(adapter);
                etSearch.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }


}