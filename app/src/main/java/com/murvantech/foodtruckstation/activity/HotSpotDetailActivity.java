package com.murvantech.foodtruckstation.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.murvantech.foodtruckstation.R;
import com.murvantech.foodtruckstation.model.TruckDetailData;
import com.murvantech.foodtruckstation.networkcall.IResult;
import com.murvantech.foodtruckstation.networkcall.URLS;
import com.murvantech.foodtruckstation.networkcall.VolleyService;
import com.murvantech.foodtruckstation.utils.Constant;
import com.murvantech.foodtruckstation.utils.MyViewPager;
import com.murvantech.foodtruckstation.utils.Preference;
import com.murvantech.foodtruckstation.utils.Utils;
import com.testfairy.TestFairy;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class HotSpotDetailActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    MyViewPager vpImage;
    TruckDetailData store;
    private IResult resultCallback;
    private VolleyService mVolleyService;
    Context context;
    private String truckID = "1";
    ImageView ivBack;
    RatingBar ratingBar;
    private TextView tvName, tvMon, tvTues, tvWed, tvThus, tvDescription, tvAddress, tvFri, tvOwnerPhone, tvSat, tvSun;


    LinearLayout llMenu, llDetail, llReview, llScheduled, llDeals, llMap;
    private GoogleMap mMap;
    private ImageView ivFav;
    Set<String> set;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TestFairy.begin(this, Constant.TEST_FAIRY_TOKEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotspot_detail);


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        llMap = findViewById(R.id.llMap);
        ivFav = findViewById(R.id.ivFav);
        llMenu = findViewById(R.id.llMenu);
        llDetail = findViewById(R.id.llDetail);
        llReview = findViewById(R.id.llReview);
        llScheduled = findViewById(R.id.llScheduled);
        llDeals = findViewById(R.id.llDeals);
        llReview.setOnClickListener(this);
        llScheduled.setOnClickListener(this);
        llDetail.setOnClickListener(this);
        llMenu.setOnClickListener(this);
        llDeals.setOnClickListener(this);
        ivFav.setOnClickListener(this);

        tvName = findViewById(R.id.tvName);
        tvMon = findViewById(R.id.tvMon);
        tvTues = findViewById(R.id.tvTues);
        tvWed = findViewById(R.id.tvWed);
        tvThus = findViewById(R.id.tvThus);
        tvFri = findViewById(R.id.tvFri);
        tvSat = findViewById(R.id.tvSat);
        tvSun = findViewById(R.id.tvSun);
        ratingBar = findViewById(R.id.ratingBar);
        tvDescription = findViewById(R.id.tvDescription);
        tvOwnerPhone = findViewById(R.id.tvOwnerPhone);
        tvAddress = findViewById(R.id.tvAddress);


        tvOwnerPhone.setOnClickListener(this);
        tvMon.setOnClickListener(this);
        tvTues.setOnClickListener(this);
        tvWed.setOnClickListener(this);

        vpImage = (MyViewPager) findViewById(R.id.vpImage);
        context = this;
        truckID = getIntent().getExtras().getString("ID");
        getDetailsWS();
        findViewById(R.id.ivBack).setOnClickListener(this);
        findViewById(R.id.ivShare).setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.ivBack:

                finish();
                break;

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (googleMap != null) {
            mMap = googleMap;
        }
    }


    private Map<String, String> getParamsLocatioin() {

        JSONObject dataa = new JSONObject();
        try {

            dataa.putOpt("lot_id", "" + truckID);
            //  dataa.putOpt("Language", Constant.LANGUAGE);
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Map<String, String> params = new HashMap<String, String>();
        // params.put("api_key", Constant.API_KEY);
        params.put("jsonData", dataa.toString());
        return params;
    }

    private void getDetailsWS() {

        initVolleyCallbackUpdate();
        mVolleyService = new VolleyService(resultCallback, context);
        mVolleyService.postStringRequest(context, "GET", URLS.LOT_DETAIL, getParamsLocatioin(), SplashActivity.TOKEN);
    }


    void initVolleyCallbackUpdate() {
        resultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, String response) {
                Log.d(Constant.TAG, " Success " + requestType);
                try {
                    JSONObject data = new JSONObject(response);


                    if (data.getString(Constant.RESPONSE_STATUS).equalsIgnoreCase(Constant.SUCCESS)) {
                        /// showToast(data.getString("response_msg"));
                        JSONObject truckDetail = data.getJSONObject("response_data");
                        JSONObject lotData = truckDetail.getJSONObject("lot_details");

                        try {
                            tvName.setText(lotData.getString("name"));
                            //tvDescription.setText("Description:-\n" + lotData.getString("description"));
                            tvAddress.setText("Address: " + lotData.getString("address_location"));
                            tvMon.setText("Monday: " + checkAvailability(lotData.getBoolean("monday_available")));
                            tvTues.setText("Tuesday: " + checkAvailability(lotData.getBoolean("tuesday_available")));
                            tvWed.setText("Wednesday: " + checkAvailability(lotData.getBoolean("wednesday_available")));
                            tvThus.setText("Thursday: " + checkAvailability(lotData.getBoolean("thursday_available")));
                            tvFri.setText("Friday: " + checkAvailability(lotData.getBoolean("friday_available")));
                            tvSat.setText("Saturday: " + checkAvailability(lotData.getBoolean("saturday_available")));
                            tvSun.setText("Sunday: " + checkAvailability(lotData.getBoolean("sunday_available")));

                            double lat = Double.parseDouble(lotData.getString("latitude"));
                            double lng = Double.parseDouble(lotData.getString("longitude"));
                            setUpMap(lat, lng);

                        } catch (Exception e) {
                            llMap.setVisibility(View.GONE);
                            e.printStackTrace();
                        }


                    } else {
                        showToast(data.getString("response_msg"));

                    }
                } catch (JSONException e) {
                    showToast(e.getLocalizedMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(Constant.TAG, " Error " + requestType);
                showToast(error.toString());

            }
        };
    }

    public String checkAvailability(boolean value) {

        if (value) {
            return "Yes";
        }

        return "No";
    }


    private void setUpMap(double lat, double lng) {

        if (mMap != null) {

            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(lat, lng))
                    .title(tvName.getText().toString())
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.ic_marker)));
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 17);
            mMap.animateCamera(cameraUpdate);
            llMap.setVisibility(View.VISIBLE);

        }
    }


    private void showToast(String localizedMessage) {
        //new MyCustomeToast().showToast(this, localizedMessage);
        Utils.showDialog(context, localizedMessage);
    }
}
