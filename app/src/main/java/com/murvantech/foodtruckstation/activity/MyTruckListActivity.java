package com.murvantech.foodtruckstation.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.murvantech.foodtruckstation.R;
import com.murvantech.foodtruckstation.model.CategoryData;
import com.murvantech.foodtruckstation.model.TruckData;
import com.murvantech.foodtruckstation.networkcall.IResult;
import com.murvantech.foodtruckstation.networkcall.URLS;
import com.murvantech.foodtruckstation.networkcall.VolleyService;
import com.murvantech.foodtruckstation.roomdb.DatabaseClient;
import com.murvantech.foodtruckstation.roomdb.Task;
import com.murvantech.foodtruckstation.utils.Constant;
import com.murvantech.foodtruckstation.utils.GPSTracker;
import com.murvantech.foodtruckstation.utils.HideKeyboard;
import com.murvantech.foodtruckstation.utils.Preference;
import com.murvantech.foodtruckstation.utils.Utils;
import com.squareup.picasso.Picasso;
import com.testfairy.TestFairy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class MyTruckListActivity extends Activity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private Gson gson = new Gson();
    private IResult resultCallback;
    private VolleyService mVolleyService;
    ArrayList<TruckData> castleList = new ArrayList<>();
    CastleListAdapter castleAdapter = new CastleListAdapter();

    SwipeRefreshLayout swipeRefreshLayout;
    LinearLayoutManager layoutManager;

    RecyclerView rvCastlelist;
    private Context context;
    private String categoryID = "1";


    private String userID = "";
    ImageView ivBack;
    TextView tvRetry;

    RecyclerView rv_category;
    CategoryAdapter categoryadapter = new CategoryAdapter();
    private ArrayList<CategoryData> categoryList = new ArrayList<>();
    private static final int NOT_SELECTED = 0;
    private int selectedPos = NOT_SELECTED;
    private GPSTracker tracker;

    private double lat = 0.0;
    private double lng = 0.0;
    private String truckID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TestFairy.begin(this, Constant.TEST_FAIRY_TOKEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_truck_list);
        new HideKeyboard().setupUI(findViewById(R.id.ll), this);
        context = this;

        userID = Preference.getInstance(context).getString(Constant.USER_ID);


        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        ivBack = findViewById(R.id.ivBack);
        swipeRefreshLayout.setOnRefreshListener(this);

        tvRetry = findViewById(R.id.tvRetry);
        rvCastlelist = findViewById(R.id.rvCastlelist);
        layoutManager = new LinearLayoutManager(context);
        rvCastlelist.setLayoutManager(layoutManager);
        rvCastlelist.setAdapter(castleAdapter);
        rvCastlelist.setVisibility(View.VISIBLE);


        rv_category = (RecyclerView) findViewById(R.id.rv_category);
        LinearLayoutManager layoutManager = new LinearLayoutManager(MyTruckListActivity.this);
        // rv_category.setLayoutManager(layoutManager);
        rv_category.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rv_category.setAdapter(categoryadapter);

        rv_category.setVisibility(View.GONE);
        // getCategory();

        ivBack.setOnClickListener(this);
        castleAdapter.notifyDataSetChanged();


        getTruckList();

    }


    private void getCategory() {

        initVolleyCallbackCategory();
        mVolleyService = new VolleyService(resultCallback, context);
        mVolleyService.getStringRequest(context, "GET", URLS.CATEGORY_LIST, getParamsDL(), SplashActivity.TOKEN);
    }

    private Map<String, String> getParamsCity() {
        JSONObject dataa = new JSONObject();
       /* try {
            dataa.putOpt("device_id", "s1a2t3i4s5f6a7i8d");
            //  dataa.putOpt("Language", Constant.LANGUAGE);
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
*/
        Map<String, String> params = new HashMap<String, String>();
        // params.put("api_key", Constant.API_KEY);
        params.put("jsonData", dataa.toString());
        return params;
    }

    void initVolleyCallbackCategory() {
        resultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, String response) {
                Log.d(Constant.TAG, " Success " + requestType);
                try {
                    JSONObject data = new JSONObject(response);

                    JSONObject responseData;


                    if (data.getString(Constant.RESPONSE_STATUS).equalsIgnoreCase(Constant.SUCCESS)) {
                        /// showToast(data.getString("response_msg"));


                        JSONArray array = data.getJSONArray("response_data");

                        categoryList.clear();


                        for (int i = 0; i < array.length(); i++) {

                            CategoryData order;
                            order = gson.fromJson(array.getJSONObject(i).toString(), CategoryData.class);

                          /*  if (i == 0) {
                                order.setId("0");
                                order.setName("All");
                                categoryList.add(order);

                            } else {
*/
                            categoryList.add(order);
                            // }

                        }
                        categoryadapter.notifyDataSetChanged();


                    } else {
                        showToast(data.getString("response_msg"));

                    }
                } catch (JSONException e) {
                    showToast(e.getLocalizedMessage());
                }
            }


            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(Constant.TAG, " Error " + requestType);
                showToast(error.toString());

            }
        };
    }


    private class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_order_category, parent, false);

            return new ViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            if (position == selectedPos) {
                // your color for selected item
                holder.tvAll.setBackgroundResource(R.drawable.category_selected_bg);
                holder.tvAll.setTextColor(getApplicationContext().getResources().getColor(R.color.white));


            } else {
                holder.tvAll.setBackgroundResource(R.drawable.category_btn_bg);
                holder.tvAll.setTextColor(getApplicationContext().getResources().getColor(R.color.txtcolordark));

            }

            final CategoryData restroArrayList = categoryList.get(position);
            Log.d("name", restroArrayList.getName());
            holder.tvAll.setText(restroArrayList.getName());


            holder.tvAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    setSelection(position);


                    categoryID = restroArrayList.getId();
                    getTruckList();


                }
            });


        }

        public void setSelection(int position) {
            if (selectedPos == position) {
                // selectedPos = NOT_SELECTED;
            } else {
                selectedPos = position;
            }
            categoryadapter.notifyDataSetChanged();
        }

        //


        @Override
        public int getItemCount() {
            return categoryList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvAll;
            ImageView ivFav;
            LinearLayout ll;

            public ViewHolder(View itemView) {
                super(itemView);
                tvAll = (TextView) itemView.findViewById(R.id.tvAll);

            }
        }

    }


    @Override
    public void onRefresh() {
        castleList.clear();
        castleAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
        getTruckList();
    }


    private void getTruckList() {
        initCallbackDL();
        mVolleyService = new VolleyService(resultCallback, context);
        mVolleyService.postStringRequest(context, "POST", URLS.MY_TRUCKS, getParamsDL(), SplashActivity.TOKEN);
    }

    private Map<String, String> getParamsDL() {
        JSONObject jsonStr = new JSONObject();


        try {
            jsonStr.put("user_id", Preference.getInstance(MyTruckListActivity.this).getString(Constant.USER_ID));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("jsonData", jsonStr.toString());
        return map;
    }

    private void initCallbackDL() {
        resultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("response_status");
                    String msg = jsonObject.getString("response_msg");

                    if (status.equalsIgnoreCase("success")) {

                        tvRetry.setVisibility(View.GONE);
                        rvCastlelist.setVisibility(View.
                                VISIBLE);
                        castleList.clear();
                        JSONObject jsonObj = jsonObject.getJSONObject("response_data");
                        JSONArray jsonArray = jsonObj.getJSONArray("trucks_list");

                        if (jsonArray.length() == 0) {
                            //tvRetry.setVisibility(View.VISIBLE);
                            swipeRefreshLayout.setRefreshing(false);
                            castleAdapter.notifyDataSetChanged();
                            tvRetry.setVisibility(View.VISIBLE);
                            rvCastlelist.setVisibility(View.
                                    GONE);


                        } else {
                            // tvRetry.setVisibility(View.GONE);

                            for (int i = 0; i < jsonArray.length(); i++) {

                                TruckData order;
                                order = gson.fromJson(jsonArray.getJSONObject(i).toString(), TruckData.class);
                                castleList.add(order);

                            }

                            //showToast("" + al_data.size());
                            swipeRefreshLayout.setRefreshing(false);
                            castleAdapter.notifyDataSetChanged();
                        }
                    } else {
                        tvRetry.setVisibility(View.VISIBLE);
                        rvCastlelist.setVisibility(View.
                                GONE);

                        Utils.dialog(context, msg);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Utils.getErrors(context, error.toString());

            }
        };
    }


    private class CastleListAdapter extends RecyclerView.Adapter<CastleListAdapter.ViewHolder> {
        @Override
        public CastleListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_my_truck_list, parent, false);
            return new CastleListAdapter.ViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(CastleListAdapter.ViewHolder holder, final int position) {
            final TruckData userData = castleList.get(position);


// set the drawable as progress drawable

            holder.tvDescription.setText(userData.getDescription());
            holder.tvName.setText(userData.getName());
            holder.tvContact.setText(userData.getPhone());


            holder.tvContact.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("MissingPermission")
                @Override
                public void onClick(View v) {


                    if (!userData.getPhone().equals("")) {

                        try {


                            Intent sIntent = new Intent(Intent.ACTION_CALL, Uri

                                    .parse("tel:" + userData.getPhone()));

                            sIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


                            startActivity(sIntent);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            try {


                String userImg = userData.getTruck_image();

                if (userImg != null && !userImg.isEmpty()) {

                    if (userImg.contains("no_image")) {
                        Picasso.with(context).load(R.drawable.logo).error(R.drawable.logo).fit().into(holder.ivUser);
                    } else {
                        Picasso.with(context).load(userImg).error(R.drawable.logo).resize(200, 200).into(holder.ivUser);
                    }
                } else {
                    Picasso.with(context).load(R.drawable.logo).error(R.drawable.logo).fit().into(holder.ivUser);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.ll_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    startActivity(new Intent(context, TruckDetailActivity.class)
                            .putExtra("ID", userData.getId()));
                   /* startActivity(new Intent(context, CastleDetailActivity.class)
                            .putExtra("DATA", restroArrayList));
*/
                }
            });


            holder.ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            holder.tvUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    truckID = userData.getId();

                    getLocation();

                }
            });

        }

        @Override
        public int getItemCount() {
            return castleList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvName, tvDescription, tvContact, tvUpdate;
            CircleImageView ivUser;
            LinearLayout ll_main;
            ImageView ivEdit;


            public ViewHolder(View itemView) {
                super(itemView);
                tvDescription = (TextView) itemView.findViewById(R.id.tvAddress);
                tvName = (TextView) itemView.findViewById(R.id.tvName);
                tvContact = (TextView) itemView.findViewById(R.id.tvContact);
                tvUpdate = (TextView) itemView.findViewById(R.id.tvUpdate);
                ivEdit = (ImageView) itemView.findViewById(R.id.ivEdit);

                ll_main = (LinearLayout) itemView.findViewById(R.id.ll);
                ivUser = (CircleImageView) itemView.findViewById(R.id.ivUser);

            }
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.ivBack:

                finish();

                break;


        }
    }

    private void getLocation() {
        tracker = new GPSTracker(MyTruckListActivity.this);
        if (tracker.canGetLocation()) {

            //  lat = 35.8533935;
            // lng = -78.7024126;
            lat = tracker.getLatitude();
            lng = tracker.getLongitude();
            if (lat != 0.0) {

                updateLocationWS();
            } else {
                Utils.showDialog(MyTruckListActivity.this, "Location not found. Please enable your GPS");
            }
        } else {

        }
    }


    private void updateLocationWS() {

        initVolleyCallbackUpdate();
        mVolleyService = new VolleyService(resultCallback, context);
        mVolleyService.postStringRequest(context, "GET", URLS.UPDATE_LOCATION, getParamsLocatioin(), SplashActivity.TOKEN);
    }

    private Map<String, String> getParamsLocatioin() {

        JSONObject dataa = new JSONObject();
        try {
            dataa.putOpt("user_id", Preference.getInstance(MyTruckListActivity.this).getString(Constant.USER_ID));
            dataa.putOpt("lat", "" + lat);
            dataa.putOpt("lng", "" + lng);
            dataa.putOpt("truck_id", "" + truckID);
            //  dataa.putOpt("Language", Constant.LANGUAGE);
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Map<String, String> params = new HashMap<String, String>();
        // params.put("api_key", Constant.API_KEY);
        params.put("jsonData", dataa.toString());
        return params;
    }

    void initVolleyCallbackUpdate() {
        resultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, String response) {
                Log.d(Constant.TAG, " Success " + requestType);
                try {
                    JSONObject data = new JSONObject(response);

                    JSONObject responseData;


                    if (data.getString(Constant.RESPONSE_STATUS).equalsIgnoreCase(Constant.SUCCESS)) {
                        /// showToast(data.getString("response_msg"));

                        getTruckList();
                        Utils.showDialog(MyTruckListActivity.this, data.getString("response_msg"));


                    } else {
                        showToast(data.getString("response_msg"));

                    }
                } catch (JSONException e) {
                    showToast(e.getLocalizedMessage());
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(Constant.TAG, " Error " + requestType);
                showToast(error.toString());

            }
        };
    }


    private void showToast(String localizedMessage) {
        //new MyCustomeToast().showToast(this, localizedMessage);
        Utils.showDialog(context, localizedMessage);
    }





}