package com.murvantech.foodtruckstation.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.murvantech.foodtruckstation.R;
import com.murvantech.foodtruckstation.model.ScheduledData;
import com.murvantech.foodtruckstation.model.ScheduledData;
import com.murvantech.foodtruckstation.networkcall.IResult;
import com.murvantech.foodtruckstation.networkcall.URLS;
import com.murvantech.foodtruckstation.networkcall.VolleyService;
import com.murvantech.foodtruckstation.utils.ChangeDateFormat;
import com.murvantech.foodtruckstation.utils.Constant;
import com.murvantech.foodtruckstation.utils.GPSTracker;
import com.murvantech.foodtruckstation.utils.HideKeyboard;
import com.murvantech.foodtruckstation.utils.Preference;
import com.murvantech.foodtruckstation.utils.Utils;
import com.testfairy.TestFairy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ScheduleListActivity extends Activity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private Gson gson = new Gson();
    private IResult resultCallback;
    private VolleyService mVolleyService;
    ArrayList<ScheduledData> ScheduledData = new ArrayList<>();
    CastleListAdapter castleAdapter = new CastleListAdapter();

    SwipeRefreshLayout swipeRefreshLayout;
    LinearLayoutManager layoutManager;

    RecyclerView rvCastlelist;
    private Context context;
    private String categoryID = "1";


    private String userID = "";
    ImageView ivBack;
    TextView tvRetry;

    private static final int NOT_SELECTED = 0;
    private int selectedPos = NOT_SELECTED;
    private GPSTracker tracker;

    private double lat = 0.0;
    private double lng = 0.0;
    private String truckID = "";
    private String day = "";
    LinearLayout llMenu, llDetail, llReview, llScheduled,llDeal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TestFairy.begin(this, Constant.TEST_FAIRY_TOKEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scheduled_list);
        new HideKeyboard().setupUI(findViewById(R.id.ll), this);
        context = this;

        truckID = getIntent().getExtras().getString("ID");

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        ivBack = findViewById(R.id.ivBack);
        swipeRefreshLayout.setOnRefreshListener(this);

        tvRetry = findViewById(R.id.tvRetry);
        rvCastlelist = findViewById(R.id.rvCastlelist);
        layoutManager = new LinearLayoutManager(context);
        rvCastlelist.setLayoutManager(layoutManager);
        rvCastlelist.setAdapter(castleAdapter);
        rvCastlelist.setVisibility(View.VISIBLE);


        ivBack.setOnClickListener(this);
        castleAdapter.notifyDataSetChanged();

        llMenu = findViewById(R.id.llMenu);
        llDetail = findViewById(R.id.llDetail);
        llReview = findViewById(R.id.llReview);
        llScheduled = findViewById(R.id.llScheduled);
        llReview.setOnClickListener(this);
        llScheduled.setOnClickListener(this);
        llDetail.setOnClickListener(this);
        llMenu.setOnClickListener(this);
        llDeal = findViewById(R.id.llDeals);

        llDeal.setOnClickListener(this);


        getTruckList();

    }


    @Override
    public void onRefresh() {
        ScheduledData.clear();
        castleAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
        getTruckList();
    }


    private void getTruckList() {
        initCallbackDL();
        mVolleyService = new VolleyService(resultCallback, context);
        mVolleyService.postStringRequest(context, "POST", URLS.SECHEDULED, getParamsDL(), SplashActivity.TOKEN);
    }

    private Map<String, String> getParamsDL() {
        JSONObject jsonStr = new JSONObject();


        try {
            jsonStr.put("truck_id", truckID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("jsonData", jsonStr.toString());
        return map;
    }

    private void initCallbackDL() {
        resultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("response_status");
                    String msg = jsonObject.getString("response_msg");

                    if (status.equalsIgnoreCase("success")) {

                        tvRetry.setVisibility(View.GONE);
                        rvCastlelist.setVisibility(View.
                                VISIBLE);
                        ScheduledData.clear();
                        JSONObject jsonObj = jsonObject.getJSONObject("response_data");
                        JSONObject jsonObj1 = jsonObj.getJSONObject("schedule");
                        //JSONArray jsonArray = new JSONArray();


                        {
                            // tvRetry.setVisibility(View.GONE);

                            if (jsonObj1.has("Monday")) {
                                JSONArray jsonMonday = jsonObj1.getJSONArray("Monday");
                                for (int i = 0; i < jsonMonday.length(); i++) {

                                    ScheduledData order;
                                    order = gson.fromJson(jsonMonday.getJSONObject(i).toString(), ScheduledData.class);

                                    if (i == 0) {
                                        order.setDay("Monday");
                                    } else {
                                        order.setDay("");
                                    }

                                    ScheduledData.add(order);

                                }
                            }

                            if (jsonObj1.has("Tuesday")) {
                                JSONArray jsonTuesday = jsonObj1.getJSONArray("Tuesday");

                                for (int i = 0; i < jsonTuesday.length(); i++) {

                                    ScheduledData order;
                                    order = gson.fromJson(jsonTuesday.getJSONObject(i).toString(), ScheduledData.class);
                                    if (i == 0) {
                                        order.setDay("Tuesday");
                                    } else {
                                        order.setDay("");
                                    }

                                    ScheduledData.add(order);

                                }
                            }

                            if (jsonObj1.has("Wednesday")) {
                                JSONArray jsonWednesday = jsonObj1.getJSONArray("Wednesday");

                                for (int i = 0; i < jsonWednesday.length(); i++) {

                                    ScheduledData order;
                                    order = gson.fromJson(jsonWednesday.getJSONObject(i).toString(), ScheduledData.class);
                                    if (i == 0) {
                                        order.setDay("Wednesday");
                                    } else {
                                        order.setDay("");
                                    }
                                    ScheduledData.add(order);

                                }
                            }

                            if (jsonObj1.has("Thursday")) {
                                JSONArray jsonThursday = jsonObj1.getJSONArray("Thursday");

                                for (int i = 0; i < jsonThursday.length(); i++) {

                                    ScheduledData order;
                                    order = gson.fromJson(jsonThursday.getJSONObject(i).toString(), ScheduledData.class);
                                    if (i == 0) {
                                        order.setDay("Thursday");
                                    } else {
                                        order.setDay("");
                                    }
                                    ScheduledData.add(order);

                                }
                            }

                            if (jsonObj1.has("Friday")) {
                                JSONArray jsonFriday = jsonObj1.getJSONArray("Friday");

                                for (int i = 0; i < jsonFriday.length(); i++) {

                                    ScheduledData order;
                                    order = gson.fromJson(jsonFriday.getJSONObject(i).toString(), ScheduledData.class);
                                    if (i == 0) {
                                        order.setDay("Friday");
                                    } else {
                                        order.setDay("");
                                    }
                                    ScheduledData.add(order);

                                }
                            }

                            if (jsonObj1.has("Saturday")) {
                                JSONArray jsonSaturday = jsonObj1.getJSONArray("Saturday");

                                for (int i = 0; i < jsonSaturday.length(); i++) {

                                    ScheduledData order;
                                    order = gson.fromJson(jsonSaturday.getJSONObject(i).toString(), ScheduledData.class);
                                    if (i == 0) {
                                        order.setDay("Saturday");
                                    } else {
                                        order.setDay("");
                                    }
                                    ScheduledData.add(order);

                                }
                            }

                            if (jsonObj1.has("Sunday")) {
                                JSONArray jsonSunday = jsonObj1.getJSONArray("Sunday");
                                for (int i = 0; i < jsonSunday.length(); i++) {

                                    ScheduledData order;
                                    order = gson.fromJson(jsonSunday.getJSONObject(i).toString(), ScheduledData.class);
                                    if (i == 0) {
                                        order.setDay("Sunday");
                                    } else {
                                        order.setDay("");
                                    }
                                    ScheduledData.add(order);

                                }
                            }

                            swipeRefreshLayout.setRefreshing(false);
                            castleAdapter.notifyDataSetChanged();

                            if (ScheduledData.size() == 0) {
                                //tvRetry.setVisibility(View.VISIBLE);
                                swipeRefreshLayout.setRefreshing(false);
                                castleAdapter.notifyDataSetChanged();
                                tvRetry.setVisibility(View.VISIBLE);
                                rvCastlelist.setVisibility(View.
                                        GONE);

                            } else {
                                swipeRefreshLayout.setRefreshing(false);
                                castleAdapter.notifyDataSetChanged();
                                tvRetry.setVisibility(View.GONE);
                                rvCastlelist.setVisibility(View.
                                        VISIBLE);
                            }


                        }
                    } else {
                        tvRetry.setVisibility(View.VISIBLE);
                        rvCastlelist.setVisibility(View.
                                GONE);

                        Utils.dialog(context, msg);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Utils.getErrors(context, error.toString());

            }
        };
    }


    private class CastleListAdapter extends RecyclerView.Adapter<CastleListAdapter.ViewHolder> {
        @Override
        public CastleListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_scheduled_list, parent, false);
            return new CastleListAdapter.ViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(final CastleListAdapter.ViewHolder holder, final int position) {
            final ScheduledData userData = ScheduledData.get(position);


            if (position % 2 == 1) {
                holder.itemView.setBackgroundColor(Color.parseColor("#FFFFFF"));
                //  holder.imageView.setBackgroundColor(Color.parseColor("#FFFFFF"));
            } else {
                holder.itemView.setBackgroundColor(Color.parseColor("#FFFAF8FD"));
                //  holder.imageView.setBackgroundColor(Color.parseColor("#FFFAF8FD"));
            }

// set the drawable as progress drawable

            if (!userData.getDay().equals("")) {
                //day = userData.getSchedule_day();
                holder.tvScheduledDay.setText(userData.getSchedule_day());
                holder.tvScheduledDay.setVisibility(View.VISIBLE);
            } else {
                holder.tvScheduledDay.setVisibility(View.GONE);

            }

            try {

                holder.tvScheduledTimeFrom.setText("Scheduled from: " + ChangeDateFormat.getDateToAnother(userData.getSchedule_time_from()));
                holder.tvScheduledTimeTo.setText("Scheduled to: " + ChangeDateFormat.getDateToAnother(userData.getSchedule_time_to()));
                holder.tvCity.setText("City: " + userData.getSchedule_city());
                holder.tvState.setText("State: " + userData.getSchedule_state());
                holder.tvAddress.setText("Address: " + userData.getSchedule_street_address());
            } catch (Exception e) {
                e.printStackTrace();
            }

         /*   holder.tvScheduledDay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  *//*  if (holder.llData.getVisibility() == View.VISIBLE) {
                        holder.llData.setVisibility(View.GONE);
                    } else {
                        holder.llData.setVisibility(View.VISIBLE);

                    }*//*
                }
            });*/


        }

        @Override
        public int getItemCount() {
            return ScheduledData.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvScheduledTimeFrom, tvScheduledTimeTo, tvCity, tvState, tvAddress, tvScheduledDay;
            LinearLayout llData;

            public ViewHolder(View itemView) {
                super(itemView);
                tvScheduledTimeTo = (TextView) itemView.findViewById(R.id.tvScheduledTimeTo);
                tvScheduledTimeFrom = (TextView) itemView.findViewById(R.id.tvScheduledTimeFrom);
                tvCity = (TextView) itemView.findViewById(R.id.tvCity);
                llData = (LinearLayout) itemView.findViewById(R.id.llData);
                tvState = (TextView) itemView.findViewById(R.id.tvState);
                tvAddress = (TextView) itemView.findViewById(R.id.tvAddress);
                tvScheduledDay = (TextView) itemView.findViewById(R.id.tvScheduledDay);


            }
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.ivBack:

                finish();

                break;

            case R.id.llMenu:

                startActivity(new Intent(context, MenuListActivity.class)
                        .putExtra("ID", truckID));
                finish();

                break;
            case R.id.llReview:

                startActivity(new Intent(context, ReviewListActivity.class)
                        .putExtra("ID", truckID));
                finish();

                break;
            case R.id.llScheduled:

              //  startActivity(new Intent(context, ScheduleListActivity.class)
            //            .putExtra("ID", truckID));

                break;
            case R.id.llDetail:
                startActivity(new Intent(context, TruckDetailActivity.class)
                        .putExtra("ID", truckID));
                finish();

                break;
            case R.id.llDeals:

                startActivity(new Intent(context, DealsListActivity.class)
                        .putExtra("ID", truckID));
                finish();

                break;


        }
    }

    private void getLocation() {
        tracker = new GPSTracker(this);
        if (tracker.canGetLocation()) {

            //  lat = 35.8533935;
            // lng = -78.7024126;
            lat = tracker.getLatitude();
            lng = tracker.getLongitude();
            if (lat != 0.0) {

                updateLocationWS();
            } else {
                Utils.showDialog(this, "Location not found. Please enable your GPS");
            }
        } else {

        }
    }


    private void updateLocationWS() {

        initVolleyCallbackUpdate();
        mVolleyService = new VolleyService(resultCallback, context);
        mVolleyService.postStringRequest(context, "GET", URLS.UPDATE_LOCATION, getParamsLocatioin(), SplashActivity.TOKEN);
    }

    private Map<String, String> getParamsLocatioin() {

        JSONObject dataa = new JSONObject();
        try {
            dataa.putOpt("user_id", Preference.getInstance(this).getString(Constant.USER_ID));
            dataa.putOpt("lat", "" + lat);
            dataa.putOpt("lng", "" + lng);
            dataa.putOpt("truck_id", "" + truckID);
            //  dataa.putOpt("Language", Constant.LANGUAGE);
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Map<String, String> params = new HashMap<String, String>();
        // params.put("api_key", Constant.API_KEY);
        params.put("jsonData", dataa.toString());
        return params;
    }

    void initVolleyCallbackUpdate() {
        resultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, String response) {
                Log.d(Constant.TAG, " Success " + requestType);
                try {
                    JSONObject data = new JSONObject(response);

                    JSONObject responseData;


                    if (data.getString(Constant.RESPONSE_STATUS).equalsIgnoreCase(Constant.SUCCESS)) {
                        /// showToast(data.getString("response_msg"));

                        getTruckList();
                        Utils.showDialog(ScheduleListActivity.this, data.getString("response_msg"));


                    } else {
                        showToast(data.getString("response_msg"));

                    }
                } catch (JSONException e) {
                    showToast(e.getLocalizedMessage());
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(Constant.TAG, " Error " + requestType);
                showToast(error.toString());

            }
        };
    }


    private void showToast(String localizedMessage) {
        //new MyCustomeToast().showToast(this, localizedMessage);
        Utils.showDialog(context, localizedMessage);
    }


}