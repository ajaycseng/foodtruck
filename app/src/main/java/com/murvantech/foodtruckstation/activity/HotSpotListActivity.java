package com.murvantech.foodtruckstation.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.murvantech.foodtruckstation.R;
import com.murvantech.foodtruckstation.model.CategoryData;
import com.murvantech.foodtruckstation.model.TruckData;
import com.murvantech.foodtruckstation.networkcall.IResult;
import com.murvantech.foodtruckstation.networkcall.URLS;
import com.murvantech.foodtruckstation.networkcall.VolleyService;
import com.murvantech.foodtruckstation.utils.Constant;
import com.murvantech.foodtruckstation.utils.GPSTracker;
import com.murvantech.foodtruckstation.utils.HideKeyboard;
import com.murvantech.foodtruckstation.utils.Preference;
import com.murvantech.foodtruckstation.utils.Utils;
import com.squareup.picasso.Picasso;
import com.testfairy.TestFairy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class HotSpotListActivity extends Activity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private Gson gson = new Gson();
    private IResult resultCallback;
    private VolleyService mVolleyService;
    ArrayList<TruckData> castleList = new ArrayList<>();
    CastleListAdapter castleAdapter = new CastleListAdapter();

    SwipeRefreshLayout swipeRefreshLayout;
    LinearLayoutManager layoutManager;

    RecyclerView rvCastlelist;
    private Context context;
    private String categoryID = "1";


    private String userID = "";
    ImageView ivBack;
    TextView tvRetry, tvTitle;

    RecyclerView rv_category;
    private static final int NOT_SELECTED = 0;
    private int selectedPos = NOT_SELECTED;
    private GPSTracker tracker;

    private double lat = 0.0;
    private double lng = 0.0;
    private String truckID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TestFairy.begin(this, Constant.TEST_FAIRY_TOKEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_truck_list);
        new HideKeyboard().setupUI(findViewById(R.id.ll), this);
        context = this;

        userID = Preference.getInstance(context).getString(Constant.USER_ID);


        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        ivBack = findViewById(R.id.ivBack);
        swipeRefreshLayout.setOnRefreshListener(this);

        tvRetry = findViewById(R.id.tvRetry);
        tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText("Hotspots");
        rvCastlelist = findViewById(R.id.rvCastlelist);
        layoutManager = new LinearLayoutManager(context);
        rvCastlelist.setLayoutManager(layoutManager);
        rvCastlelist.setAdapter(castleAdapter);
        rvCastlelist.setVisibility(View.VISIBLE);


        ivBack.setOnClickListener(this);
        castleAdapter.notifyDataSetChanged();


    }


    @Override
    public void onRefresh() {
        castleList.clear();
        castleAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
        getTruckList();
    }


    private void getTruckList() {
        initCallbackDL();
        mVolleyService = new VolleyService(resultCallback, context);
        mVolleyService.postStringRequest(context, "POST", URLS.HOT_SPOT, getParamsDL(), SplashActivity.TOKEN);
    }

    private Map<String, String> getParamsDL() {
        JSONObject jsonStr = new JSONObject();


        try {
            jsonStr.put("lat", lat);
            jsonStr.put("lng", lng);
            // jsonStr.put("user_id", Preference.getInstance(HotSpotListActivity.this).getString(Constant.USER_ID));


        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("jsonData", jsonStr.toString());
        return map;
    }

    private void initCallbackDL() {
        resultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("response_status");
                    String msg = jsonObject.getString("response_msg");

                    if (status.equalsIgnoreCase("success")) {

                        tvRetry.setVisibility(View.GONE);
                        rvCastlelist.setVisibility(View.
                                VISIBLE);
                        castleList.clear();
                        //   JSONObject jsonObj = jsonObject.getJSONObject("response_data");
                        JSONArray jsonArray = jsonObject.getJSONArray("response_data");

                        if (jsonArray.length() == 0) {
                            //tvRetry.setVisibility(View.VISIBLE);
                            swipeRefreshLayout.setRefreshing(false);
                            castleAdapter.notifyDataSetChanged();
                            tvRetry.setVisibility(View.VISIBLE);
                            rvCastlelist.setVisibility(View.
                                    GONE);


                        } else {
                            // tvRetry.setVisibility(View.GONE);

                            for (int i = 0; i < jsonArray.length(); i++) {

                                TruckData order;
                                order = gson.fromJson(jsonArray.getJSONObject(i).toString(), TruckData.class);
                                castleList.add(order);

                            }

                            //showToast("" + al_data.size());
                            swipeRefreshLayout.setRefreshing(false);
                            castleAdapter.notifyDataSetChanged();
                        }
                    } else {
                        tvRetry.setVisibility(View.VISIBLE);
                        rvCastlelist.setVisibility(View.
                                GONE);

                        Utils.dialog(context, msg);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    tvRetry.setVisibility(View.VISIBLE);
                    tvRetry.setText("No Near hot spots!!");
                    rvCastlelist.setVisibility(View.
                            GONE);
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Utils.getErrors(context, error.toString());

            }
        };
    }


    private class CastleListAdapter extends RecyclerView.Adapter<CastleListAdapter.ViewHolder> {
        @Override
        public CastleListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_hot_spot, parent, false);
            return new CastleListAdapter.ViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(CastleListAdapter.ViewHolder holder, final int position) {
            final TruckData userData = castleList.get(position);


// set the drawable as progress drawable


            try {
                holder.tvDescription.setText(userData.getDescription());
                holder.tvName.setText(userData.getName());
                holder.tvAddress.setText(userData.getAddress_location());
                holder.tvDescription.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
            }


            holder.ll_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    startActivity(new Intent(context, HotSpotDetailActivity.class)
                            .putExtra("ID", userData.getId()));
                   /* startActivity(new Intent(context, CastleDetailActivity.class)
                            .putExtra("DATA", restroArrayList));
*/
                }
            });


            holder.ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            holder.tvUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    truckID = userData.getId();

                    getLocation();

                }
            });

        }

        @Override
        public int getItemCount() {
            return castleList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvName, tvDescription, tvAddress, tvUpdate;
            LinearLayout ll_main;
            ImageView ivEdit;


            public ViewHolder(View itemView) {
                super(itemView);
                tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
                tvName = (TextView) itemView.findViewById(R.id.tvName);
                tvAddress = (TextView) itemView.findViewById(R.id.tvAddress);
                tvUpdate = (TextView) itemView.findViewById(R.id.tvUpdate);
                ivEdit = (ImageView) itemView.findViewById(R.id.ivEdit);
                ll_main = (LinearLayout) itemView.findViewById(R.id.ll);

            }
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.ivBack:

                finish();

                break;


        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        getLocation();
    }

    private void getLocation() {
        tracker = new GPSTracker(HotSpotListActivity.this);
        if (tracker.canGetLocation()) {

            //  lat = 35.8533935;
            // lng = -78.7024126;
            lat = tracker.getLatitude();
            lng = tracker.getLongitude();


            if (lat != 0.0) {
                getTruckList();
            }

        }
    }


    private void showToast(String localizedMessage) {
        //new MyCustomeToast().showToast(this, localizedMessage);
        Utils.showDialog(context, localizedMessage);
    }


}