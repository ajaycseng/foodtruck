package com.murvantech.foodtruckstation.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.provider.SyncStateContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.murvantech.foodtruckstation.R;
import com.murvantech.foodtruckstation.utils.Constant;
import com.murvantech.foodtruckstation.utils.Preference;
import com.testfairy.TestFairy;

public class SplashActivity extends AppCompatActivity {

    public static final String TOKEN = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TestFairy.begin(this, Constant.TEST_FAIRY_TOKEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Permission();

    }


    public void Permission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                1);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            if (Preference.getInstance(SplashActivity.this).getString(Constant.USER_ID).equals("")) {
                                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                            } else {
                                HomeActivity.page = "MyTrucks";
                                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                            }

                            finish();
                            //Do something after 100ms
                        }
                    }, 2000);


                } else {
                    // finish();
                    Toast.makeText(this, "Permission denied to read your Location.", Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }
    }


}




