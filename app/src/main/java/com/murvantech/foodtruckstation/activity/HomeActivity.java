package com.murvantech.foodtruckstation.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.murvantech.foodtruckstation.R;
import com.murvantech.foodtruckstation.model.CategoryData;
import com.murvantech.foodtruckstation.model.TruckData;
import com.murvantech.foodtruckstation.model.TruckDetailData;
import com.murvantech.foodtruckstation.model.UserData;
import com.murvantech.foodtruckstation.networkcall.IResult;
import com.murvantech.foodtruckstation.networkcall.URLS;
import com.murvantech.foodtruckstation.networkcall.VolleyService;
import com.murvantech.foodtruckstation.utils.Constant;
import com.murvantech.foodtruckstation.utils.GPSTracker;
import com.murvantech.foodtruckstation.utils.HideKeyboard;
import com.murvantech.foodtruckstation.utils.Preference;
import com.murvantech.foodtruckstation.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class HomeActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    TextView tvUserName;
    ImageView ivUser;
    RecyclerView rvSideMenu;
    ArrayList<String> navItems = new ArrayList<>();
    AdapterClass mAdapter;
    ActionBarDrawerToggle toggle;
    FrameLayout main_layout, fragContainer;
    LinearLayout slideMenu;
    private String userID = "";
    DrawerLayout dlMain;
    ImageView ivMenu;

    private Gson gson = new Gson();
    public static String TOKEN = "";
    public static boolean IS_FOR_USER = false;
    public static String page = "home";


    private IResult resultCallback;
    private VolleyService mVolleyService;
    ArrayList<TruckData> castleList = new ArrayList<>();
    CastleListAdapter castleAdapter = new CastleListAdapter();

    SwipeRefreshLayout swipeRefreshLayout;
    LinearLayoutManager layoutManager;

    RecyclerView rvCastlelist;
    private Context context;
    private String categoryID = "1";


    TextView tvRetry;

    RecyclerView rv_category;
    CategoryAdapter categoryadapter = new CategoryAdapter();
    private ArrayList<CategoryData> categoryList = new ArrayList<>();
    private static final int NOT_SELECTED = 0;
    private int selectedPos = NOT_SELECTED;
    static Dialog dialog;
    private GPSTracker tracker;
    private double lat = 0.0;
    private double lng = 0.0;

    ImageView ivSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        if (page.equals("MyTrucks")) {
            startActivity(new Intent(this, MyTruckListActivity.class));

        }
        new HideKeyboard().setupUI(findViewById(R.id.dlMain), this);
        ivMenu = findViewById(R.id.ivMenu);
        tvUserName = findViewById(R.id.tvUserName);
        rvSideMenu = findViewById(R.id.rvSideMenu);

        ivSearch = findViewById(R.id.ivSearch);
        ivSearch.setOnClickListener(this);
        ivUser = findViewById(R.id.ivUser);
        dlMain = (DrawerLayout) findViewById(R.id.dlMain);
        main_layout = (FrameLayout) findViewById(R.id.main_layout);
        fragContainer = (FrameLayout) findViewById(R.id.fragContainer);
        toggle = new ActionBarDrawerToggle(this, dlMain, R.string.close_drawer, R.string.open_drawer) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);

                // setUserDATA();
                main_layout.setTranslationX(slideOffset * drawerView.getWidth());
                dlMain.bringChildToFront(drawerView);
                dlMain.requestLayout();
            }
        };
        dlMain.addDrawerListener(toggle);
        ivMenu.setOnClickListener(this);
        ivUser.setOnClickListener(this);


        slideMenu = (LinearLayout) findViewById(R.id.slideMenu);
        userID = Preference.getInstance(HomeActivity.this).getString(Constant.USER_ID);

        if (userID.equals("")) {
            navItems.add("Near By Trucks");
            navItems.add("Truck Owner's Login");
            navItems.add("Hotspots");
            navItems.add("Favorite");

            navItems.add("Exit");
        } else {
            navItems.add("Near By Trucks");
            navItems.add("My Trucks");
            navItems.add("Hotspots");
            navItems.add("Favorite");

            navItems.add("Terms and Conditions");
            navItems.add("Logout");
            setUserDATA();
        }

        mAdapter = new AdapterClass(navItems);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        LinearLayoutManager layoutManagerPost = new LinearLayoutManager(this);
        rvSideMenu.setLayoutManager(layoutManager);
        rvSideMenu.setItemAnimator(new DefaultItemAnimator());
        rvSideMenu.setAdapter(mAdapter);


        new HideKeyboard().setupUI(findViewById(R.id.ll), this);
        context = this;

        userID = Preference.getInstance(context).getString(Constant.USER_ID);


        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);

        tvRetry = findViewById(R.id.tvRetry);
        rvCastlelist = findViewById(R.id.rvCastlelist);
        layoutManager = new LinearLayoutManager(context);
        rvCastlelist.setLayoutManager(layoutManager);
        rvCastlelist.setAdapter(castleAdapter);
        rvCastlelist.setVisibility(View.VISIBLE);


        rv_category = (RecyclerView) findViewById(R.id.rv_category);
        LinearLayoutManager layoutManagercate = new LinearLayoutManager(HomeActivity.this);
        // rv_category.setLayoutManager(layoutManagercate);
        rv_category.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rv_category.setAdapter(categoryadapter);
        getCategory();

        castleAdapter.notifyDataSetChanged();


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.ivMenu:

                dlMain.openDrawer(slideMenu);

                break;

            case R.id.ivUser:


                break;
            case R.id.ivSearch:

                startActivity(new Intent(HomeActivity.this, SearchActivity.class));


                break;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        //setUserDATA();
        getLocation();

    }

    private void setUserDATA() {
        try {
            String userDetail = Preference.getInstance(HomeActivity.this).getString(Constant.USER_DETAIL);
            JSONObject jsonObject = new JSONObject(userDetail);
            UserData user;
            user = gson.fromJson(jsonObject.toString(), UserData.class);
            //   tvUserName.setText(user.getFirst_name());
            //  String img = Preference.getInstance(HomeActivity.this).getString(Constant.USER_IMG);
            // String firstName = Preference.getInstance(HomeActivity.this).getString(Constant.USER_FIRST_NAME);
            tvUserName.setText(user.getFirst_name());

            //  if (!img.equals("")) {
            //       Picasso.with(HomeActivity.this).load(URLS.IMG_BASE_URL + img).error(R.drawable.user_profile).into(ivUser);
            //   }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    class AdapterClass extends RecyclerView.Adapter<AdapterClass.MyViewHolder> {
        private ArrayList<String> navItems;

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tvName;
            ImageView iv;
            LinearLayout ll;

            MyViewHolder(View view) {
                super(view);
                tvName = (TextView) view.findViewById(R.id.tvName);
                iv = (ImageView) view.findViewById(R.id.iv);
                ll = (LinearLayout) view.findViewById(R.id.ll);
            }
        }

        AdapterClass(ArrayList<String> navItems) {
            this.navItems = navItems;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_nav_menu, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {

            holder.tvName.setText(navItems.get(position));
            if (userID.equals("")) {
                if (position == 0) {
                    holder.iv.setImageResource(R.drawable.ic_nearby);
                } else if (position == 1) {
                    holder.iv.setImageResource(R.drawable.ic_login);
                } else if (position == 2) {
                    holder.iv.setImageResource(R.drawable.ic_spot);
                } else if (position == 3) {
                    holder.iv.setImageResource(R.drawable.ic_like);
                } else if (position == 4) {
                    holder.iv.setImageResource(R.drawable.ic_exit);
                }
                holder.ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dlMain.isDrawerOpen(slideMenu))
                            dlMain.closeDrawer(slideMenu);
                        if (position == 0) {
                            // Intent intent = new Intent(HomeActivity.this, My.class);
                            // startActivity(intent);
                        } else if (position == 1) {
                            Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                            startActivity(intent);
                        } else if (position == 2) {
                            Intent intent = new Intent(HomeActivity.this, HotSpotListActivity.class);
                            startActivity(intent);
                        } else if (position == 3) {
                            Intent intent = new Intent(HomeActivity.this, FavoriteActivity.class);
                            startActivity(intent);
                        } else if (position == 4) {

                            finish();
                        }
                    }
                });
            } else {


                if (position == 0) {
                    holder.iv.setImageResource(R.drawable.ic_nearby);
                    // replaceFragment(HomeFragment.newInstance());
                } else if (position == 1) {
                    holder.iv.setImageResource(R.drawable.ic_truck);

                } else if (position == 2) {
                    holder.iv.setImageResource(R.drawable.ic_spot);
                } else if (position == 3) {
                    holder.iv.setImageResource(R.drawable.ic_like);
                } else if (position == 4) {
                    holder.iv.setImageResource(R.drawable.ic_terms);
                } else if (position == 5) {
                    holder.iv.setImageResource(R.drawable.ic_logout);
                }
                holder.ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dlMain.isDrawerOpen(slideMenu))
                            dlMain.closeDrawer(slideMenu);
                        if (position == 0) {


                        } else if (position == 1) {

                            Intent intent = new Intent(HomeActivity.this, MyTruckListActivity.class);
                            startActivity(intent);

                        } else if (position == 2) {

                            Intent intent = new Intent(HomeActivity.this, HotSpotListActivity.class);
                            startActivity(intent);


                        } else if (position == 3) {

                            Intent intent = new Intent(HomeActivity.this, FavoriteActivity.class);
                            startActivity(intent);


                        } else if (position == 4) {

                            Intent intent = new Intent(HomeActivity.this, PrivacyActivity.class);
                            startActivity(intent);


                        } else if (position == 4) {

                            logout(HomeActivity.this);

                        }

                    }
                });
            }

        }

        @Override
        public int getItemCount() {
            return navItems.size();
        }
    }


    private void logout(final Context context) {
        dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // dialog.setTitle("Please select an Event !");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        int screenWidth = display.getWidth();
        // int screenHeight = display.getHeight();

        dialog.setContentView(R.layout.dialog_msg);

        TextView tv_msg = (TextView) dialog.findViewById(R.id.tv_msg);
        TextView btnOK = (TextView) dialog.findViewById(R.id.btn_submit);
        TextView btnNo = (TextView) dialog.findViewById(R.id.btn_cancel);
        btnNo.setVisibility(View.VISIBLE);
        tv_msg.setText("Do you want to Logout?");
        btnOK.setText("Yes");
        btnNo.setText("No");
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                HomeActivity.TOKEN = "";
                Preference.getInstance(HomeActivity.this).putString(Constant.USER_ID, "");
                // Preference.getInstance(HomeActivity.this).putString(Constant.TOKEN, "");
                Preference.getInstance(HomeActivity.this).putString(Constant.USER_DETAIL, "");
                page = "home";
                startActivity(new Intent(HomeActivity.this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));

            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setLayout((int) (screenWidth / 1.1), LinearLayout.LayoutParams.WRAP_CONTENT);

    }


    private void getCategory() {

        initVolleyCallbackCategory();
        mVolleyService = new VolleyService(resultCallback, context);
        mVolleyService.getStringRequest(context, "GET", URLS.CATEGORY_LIST, getParamsDL(), SplashActivity.TOKEN);
    }

    private Map<String, String> getParamsCity() {
        JSONObject dataa = new JSONObject();
       /* try {
            dataa.putOpt("device_id", "s1a2t3i4s5f6a7i8d");
            //  dataa.putOpt("Language", Constant.LANGUAGE);
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
*/
        Map<String, String> params = new HashMap<String, String>();
        // params.put("api_key", Constant.API_KEY);
        params.put("jsonData", dataa.toString());
        return params;
    }

    void initVolleyCallbackCategory() {
        resultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, String response) {
                Log.d(Constant.TAG, " Success " + requestType);
                try {
                    JSONObject data = new JSONObject(response);

                    JSONObject responseData;


                    if (data.getString(Constant.RESPONSE_STATUS).equalsIgnoreCase(Constant.SUCCESS)) {
                        /// showToast(data.getString("response_msg"));


                        JSONArray array = data.getJSONArray("response_data");

                        categoryList.clear();


                        for (int i = 0; i < array.length(); i++) {

                            CategoryData order;
                            order = gson.fromJson(array.getJSONObject(i).toString(), CategoryData.class);

                          /*  if (i == 0) {
                                order.setId("0");
                                order.setName("All");
                                categoryList.add(order);

                            } else {
*/
                            categoryList.add(order);
                            // }

                        }
                        categoryadapter.notifyDataSetChanged();


                    } else {
                        showToast(data.getString("response_msg"));

                    }
                } catch (JSONException e) {
                    showToast(e.getLocalizedMessage());
                }
            }


            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(Constant.TAG, " Error " + requestType);
                showToast(error.toString());

            }
        };
    }


    private class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
        @Override
        public CategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_order_category, parent, false);

            return new CategoryAdapter.ViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(final CategoryAdapter.ViewHolder holder, final int position) {
            if (position == selectedPos) {
                // your color for selected item
                holder.tvAll.setBackgroundResource(R.drawable.category_selected_bg);
                holder.tvAll.setTextColor(getApplicationContext().getResources().getColor(R.color.white));


            } else {
                holder.tvAll.setBackgroundResource(R.drawable.category_btn_bg);
                holder.tvAll.setTextColor(getApplicationContext().getResources().getColor(R.color.txtcolordark));

            }

            final CategoryData restroArrayList = categoryList.get(position);
            Log.d("name", restroArrayList.getName());
            holder.tvAll.setText(restroArrayList.getName());


            holder.tvAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    setSelection(position);


                    categoryID = restroArrayList.getId();
                    getTruckList();


                }
            });


        }

        public void setSelection(int position) {
            if (selectedPos == position) {
                // selectedPos = NOT_SELECTED;
            } else {
                selectedPos = position;
            }
            categoryadapter.notifyDataSetChanged();
        }

        //


        @Override
        public int getItemCount() {
            return categoryList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvAll;
            ImageView ivFav;
            LinearLayout ll;

            public ViewHolder(View itemView) {
                super(itemView);
                tvAll = (TextView) itemView.findViewById(R.id.tvAll);

            }
        }

    }


    @Override
    public void onRefresh() {
        castleList.clear();
        castleAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
        getTruckList();
    }


    private void getTruckList() {
        initCallbackDL();
        mVolleyService = new VolleyService(resultCallback, context);
        mVolleyService.postStringRequest(context, "GET", URLS.TRUCK_LIST, getParamsDL(), SplashActivity.TOKEN);
    }

    private Map<String, String> getParamsDL() {
        JSONObject jsonStr = new JSONObject();


        try {
            jsonStr.put("category_id", categoryID);
            jsonStr.put("lat", "" + lat);
            jsonStr.put("lng", "" + lng);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("jsonData", jsonStr.toString());
        return map;
    }

    private void initCallbackDL() {
        resultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("response_status");
                    String msg = jsonObject.getString("response_msg");

                    if (status.equalsIgnoreCase("success")) {

                        tvRetry.setVisibility(View.GONE);
                        rvCastlelist.setVisibility(View.
                                VISIBLE);
                        castleList.clear();
                        JSONObject jsonObj = jsonObject.getJSONObject("response_data");
                        JSONArray jsonArray = jsonObj.getJSONArray("trucks_list");

                        if (jsonArray.length() == 0) {
                            //tvRetry.setVisibility(View.VISIBLE);
                            swipeRefreshLayout.setRefreshing(false);
                            castleAdapter.notifyDataSetChanged();
                            tvRetry.setVisibility(View.VISIBLE);
                            rvCastlelist.setVisibility(View.
                                    GONE);


                        } else {
                            // tvRetry.setVisibility(View.GONE);

                            for (int i = 0; i < jsonArray.length(); i++) {

                                TruckData order;
                                order = gson.fromJson(jsonArray.getJSONObject(i).toString(), TruckData.class);
                                castleList.add(order);

                            }

                            //showToast("" + al_data.size());
                            swipeRefreshLayout.setRefreshing(false);
                            castleAdapter.notifyDataSetChanged();
                        }
                    } else {
                        tvRetry.setVisibility(View.VISIBLE);
                        rvCastlelist.setVisibility(View.
                                GONE);

                        Utils.dialog(context, msg);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Utils.getErrors(context, error.toString());

            }
        };
    }


    private class CastleListAdapter extends RecyclerView.Adapter<CastleListAdapter.ViewHolder> {
        @Override
        public CastleListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_truck_list, parent, false);
            return new CastleListAdapter.ViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(CastleListAdapter.ViewHolder holder, final int position) {
            final TruckData userData = castleList.get(position);


// set the drawable as progress drawable

            holder.tvDescription.setText(userData.getDescription());
            holder.tvName.setText(userData.getName());
            holder.tvContact.setText(userData.getPhone());


            holder.ivCall.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("MissingPermission")
                @Override
                public void onClick(View v) {


                    if (!userData.getPhone().equals("")) {

                        try {


                            Intent sIntent = new Intent(Intent.ACTION_CALL, Uri

                                    .parse("tel:" + userData.getPhone()));

                            sIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


                            startActivity(sIntent);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            try {


                String userImg = userData.getTruck_image();


                if (userImg != null && !userImg.isEmpty()) {
                    if (userImg.contains("no_image")) {
                        Picasso.with(context).load(R.drawable.logo).error(R.drawable.logo).fit().into(holder.ivUser);

                    } else {

                        Picasso.with(context).load(userImg).error(R.drawable.logo).resize(200, 200).into(holder.ivUser);
                    }

                } else {
                    Picasso.with(context).load(R.drawable.logo).error(R.drawable.logo).fit().into(holder.ivUser);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.ll_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    startActivity(new Intent(context, TruckDetailActivity.class)
                            .putExtra("ID", userData.getId()));
                }
            });

        }

        @Override
        public int getItemCount() {
            return castleList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvName, tvDescription, tvContact;
            CircleImageView ivUser;
            ImageView ivCall;
            LinearLayout ll_main;


            public ViewHolder(View itemView) {
                super(itemView);
                tvDescription = (TextView) itemView.findViewById(R.id.tvAddress);
                tvName = (TextView) itemView.findViewById(R.id.tvName);
                ivCall = itemView.findViewById(R.id.ivCall);
                tvContact = (TextView) itemView.findViewById(R.id.tvContact);

                ll_main = (LinearLayout) itemView.findViewById(R.id.ll);
                ivUser = (CircleImageView) itemView.findViewById(R.id.ivUser);

            }
        }
    }


    private void getLocation() {
        tracker = new GPSTracker(HomeActivity.this);
        if (tracker.canGetLocation()) {

            //  lat = 35.8533935;
            // lng = -78.7024126;
            lat = tracker.getLatitude();
            lng = tracker.getLongitude();
            if (lat != 0.0) {

                getTruckList();
            } else {
                Utils.showDialog(HomeActivity.this, "Location not found. Please enable your GPS");
            }
        } else {

        }
    }

    private void showToast(String localizedMessage) {
        //new MyCustomeToast().showToast(this, localizedMessage);
        Utils.showDialog(context, localizedMessage);
    }

}
