package com.murvantech.foodtruckstation.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.murvantech.foodtruckstation.R;
import com.murvantech.foodtruckstation.roomdb.DatabaseClient;
import com.murvantech.foodtruckstation.roomdb.Task;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class FavoriteActivity extends AppCompatActivity {

    RecyclerView recycleView;
    List<Task> tasks = new ArrayList<>();
    FavAdapter favAdapter = new FavAdapter();
    TextView tvRetry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        recycleView = findViewById(R.id.recycleView);
        tvRetry = findViewById(R.id.tvRetry);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        // rv_category.setLayoutManager(layoutManager);
        recycleView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recycleView.setAdapter(favAdapter);


        findViewById(R.id.ivBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getTasks();
    }

    private void getTasks() {
        class GetTasks extends AsyncTask<Void, Void, List<Task>> {

            @Override
            protected List<Task> doInBackground(Void... voids) {
                tasks = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .getAll();
                return tasks;
            }

            @Override
            protected void onPostExecute(List<Task> tasks) {
                super.onPostExecute(tasks);

                if (tasks.size() == 0) {
                    tvRetry.setVisibility(View.VISIBLE);
                    return;
                }


                recycleView.setAdapter(favAdapter);
                favAdapter.notifyDataSetChanged();
            }
        }

        GetTasks gt = new GetTasks();
        gt.execute();
    }


    private class FavAdapter extends RecyclerView.Adapter<FavAdapter.ViewHolder> {


        @Override
        public FavAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_my_fav, parent, false);
            return new FavAdapter.ViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(FavAdapter.ViewHolder holder, final int position) {
            final Task userData = tasks.get(position);


            holder.tvDescription.setText(userData.getDescription());
            holder.tvName.setText(userData.getName());
            holder.tvContact.setText(userData.getPhone());

            try {


                if (userData.getImage() != null && !userData.getImage().isEmpty()) {

                    if (userData.getImage().contains("no_image")) {
                        Picasso.with(FavoriteActivity.this).load(R.drawable.logo).error(R.drawable.logo).fit().into(holder.ivUser);
                    } else {
                        Picasso.with(FavoriteActivity.this).load(userData.getImage()).error(R.drawable.logo).resize(200, 200).into(holder.ivUser);
                    }
                } else {
                    Picasso.with(FavoriteActivity.this).load(R.drawable.logo).error(R.drawable.logo).fit().into(holder.ivUser);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            holder.tvContact.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("MissingPermission")
                @Override
                public void onClick(View v) {


                    if (!userData.getPhone().equals("")) {

                        try {


                            Intent sIntent = new Intent(Intent.ACTION_CALL, Uri

                                    .parse("tel:" + userData.getPhone()));

                            sIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


                            startActivity(sIntent);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });


            holder.ll_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    startActivity(new Intent(FavoriteActivity.this, TruckDetailActivity.class)
                            .putExtra("ID", userData.getId()));

                }
            });

            holder.ivUnFav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    tasks.remove(position);;;;;;;
                    deleteFav(userData);

                }
            });


            holder.ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });


        }

        @Override
        public int getItemCount() {
            return tasks.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvName, tvDescription, tvContact;
            CircleImageView ivUser;
            LinearLayout ll_main;
            ImageView ivEdit, ivUnFav;


            public ViewHolder(View itemView) {
                super(itemView);
                tvDescription = itemView.findViewById(R.id.tvAddress);
                tvName = itemView.findViewById(R.id.tvName);
                tvContact = itemView.findViewById(R.id.tvContact);
                ivEdit = itemView.findViewById(R.id.ivEdit);
                ivUnFav = itemView.findViewById(R.id.ivUnFav);

                ll_main = itemView.findViewById(R.id.ll);
                ivUser = itemView.findViewById(R.id.ivUser);

            }
        }
    }

    private void deleteFav(final Task task) {


        class SaveTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {


                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .taskDao()
                        .delete(task);

                return null;

            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                favAdapter.notifyDataSetChanged();
            }
        }

        SaveTask st = new SaveTask();
        st.execute();
    }


}
