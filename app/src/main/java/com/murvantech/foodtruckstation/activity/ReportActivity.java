package com.murvantech.foodtruckstation.activity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.murvantech.foodtruckstation.R;
import com.murvantech.foodtruckstation.model.TruckDetailData;
import com.murvantech.foodtruckstation.networkcall.IResult;
import com.murvantech.foodtruckstation.networkcall.URLS;
import com.murvantech.foodtruckstation.networkcall.VolleyService;
import com.murvantech.foodtruckstation.utils.Constant;
import com.murvantech.foodtruckstation.utils.Preference;
import com.murvantech.foodtruckstation.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ReportActivity extends AppCompatActivity {


    private EditText etMessage, etType;
    private IResult resultCallback;
    private VolleyService mVolleyService;
    TruckDetailData truckDetailData;
    private TextView tvTuck, tvDate, tvDescription;
    private String type, message;
    String[] items = {"Incorrect location", "Incorrect information"};
    private int typePosition = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        etMessage = findViewById(R.id.etMessage);
        etType = findViewById(R.id.etType);
        tvTuck = findViewById(R.id.tvTuck);
        tvDate = findViewById(R.id.tvDate);
        tvDescription = findViewById(R.id.tvDescription);
        etType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTypeChoice();
            }
        });

        findViewById(R.id.ivBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        truckDetailData = (TruckDetailData) getIntent().getExtras().getSerializable("DATA");

        if (truckDetailData != null) {
            tvTuck.setText(truckDetailData.getName());
            tvDate.setText(truckDetailData.getAddress());
            tvDescription.setText(truckDetailData.getDescription());


        }
        findViewById(R.id.btnSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                message = etMessage.getText().toString().trim();
                type = etType.getText().toString().trim();


                if (TextUtils.isEmpty(type)) {
                    showToast("Write issue type");

                } else if (TextUtils.isEmpty(message)) {
                    showToast("Describe your issue");

                } else {
                    updateLocationWS();
                }


            }
        });
    }

    private void dialogTypeChoice() {

        new AlertDialog.Builder(this)
                .setSingleChoiceItems(items, typePosition, null)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        typePosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        etType.setText(items[typePosition]);
                        // Do something useful withe the position of the selected radio button
                    }
                })
                .show();
    }


    private void updateLocationWS() {

        initVolleyCallReportLocation();
        mVolleyService = new VolleyService(resultCallback, this);
        mVolleyService.postStringRequest(this, "POST", URLS.REPORT_LOCATION, getParamsLocatioReport(), SplashActivity.TOKEN);
    }

    private Map<String, String> getParamsLocatioReport() {

        JSONObject dataa = new JSONObject();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");

        String currentDate = sdf.format(new Date());
        String currentTime = sdf1.format(new Date());


        try {
            dataa.putOpt("user_id", Preference.getInstance(ReportActivity.this).getString(Constant.USER_ID));
            dataa.putOpt("lat", "" + truckDetailData.getLat());
            dataa.putOpt("lng", "" + truckDetailData.getLng());
            dataa.putOpt("truck_id", "" + truckDetailData.getId());
            dataa.putOpt("truck_name", "" + truckDetailData.getName());
            dataa.putOpt("issue_type", "" + type);
            dataa.putOpt("current_date", "" + currentDate);
            dataa.putOpt("current_time", "" + currentTime);
            dataa.putOpt("message", "" + message);
            dataa.putOpt("device_type", "Android");
            dataa.putOpt("device_id", "food123");


            //  dataa.putOpt("Language", Constant.LANGUAGE);
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Map<String, String> params = new HashMap<String, String>();
        // params.put("api_key", Constant.API_KEY);
        params.put("jsonData", dataa.toString());
        return params;
    }

    void initVolleyCallReportLocation() {
        resultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, String response) {
                Log.d(Constant.TAG, " Success " + requestType);
                try {
                    JSONObject data = new JSONObject(response);


                    if (data.getString(Constant.RESPONSE_STATUS).equalsIgnoreCase(Constant.SUCCESS)) {
                        /// showToast(data.getString("response_msg"));
                        Utils.showDialogAction(ReportActivity.this, "Thanks for reporting the " + type, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        });


                    } else {
                        showToast(data.getString("response_msg"));

                    }
                } catch (JSONException e) {
                    showToast(e.getLocalizedMessage());
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(Constant.TAG, " Error " + requestType);
                showToast(error.toString());

            }
        };
    }

    private void showToast(String localizedMessage) {
        //new MyCustomeToast().showToast(this, localizedMessage);
        Utils.showDialog(ReportActivity.this, localizedMessage);
    }

}
