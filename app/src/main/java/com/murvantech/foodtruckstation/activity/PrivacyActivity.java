package com.murvantech.foodtruckstation.activity;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.murvantech.foodtruckstation.R;
import com.murvantech.foodtruckstation.networkcall.IResult;
import com.murvantech.foodtruckstation.networkcall.VolleyService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class PrivacyActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView ivBack;

    private WebView webView;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_privacy);
        ivBack = (ImageView) findViewById(R.id.ivBack);
        webView = findViewById(R.id.wvWebView);


        ivBack.setOnClickListener(this);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        WebViewClient webViewClient = new WebViewClient();
        webView.setWebViewClient(webViewClient);


        webView.loadUrl("http://foodtruckstationclt.com/pages/terms");


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBack:

                finish();

                break;
        }
    }
}
