package com.murvantech.foodtruckstation.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.murvantech.foodtruckstation.R;
import com.murvantech.foodtruckstation.model.ReviewData;
import com.murvantech.foodtruckstation.networkcall.IResult;
import com.murvantech.foodtruckstation.networkcall.URLS;
import com.murvantech.foodtruckstation.networkcall.VolleyService;
import com.murvantech.foodtruckstation.utils.Constant;
import com.murvantech.foodtruckstation.utils.GPSTracker;
import com.murvantech.foodtruckstation.utils.HideKeyboard;
import com.murvantech.foodtruckstation.utils.Preference;
import com.murvantech.foodtruckstation.utils.Utils;
import com.testfairy.TestFairy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ReviewListActivity extends Activity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private Gson gson = new Gson();
    private IResult resultCallback;
    private VolleyService mVolleyService;
    ArrayList<ReviewData> reviewData = new ArrayList<>();
    CastleListAdapter castleAdapter = new CastleListAdapter();

    SwipeRefreshLayout swipeRefreshLayout;
    LinearLayoutManager layoutManager;

    RecyclerView rvCastlelist;
    private Context context;
    private String categoryID = "1";


    private String userID = "";
    ImageView ivBack;
    TextView tvRetry;

    private static final int NOT_SELECTED = 0;
    private int selectedPos = NOT_SELECTED;
    private GPSTracker tracker;

    private double lat = 0.0;
    private double lng = 0.0;
    private String truckID = "";
    LinearLayout llMenu, llDetail, llReview, llScheduled, llDeal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TestFairy.begin(this, Constant.TEST_FAIRY_TOKEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_list);
        new HideKeyboard().setupUI(findViewById(R.id.ll), this);
        context = this;

        truckID = getIntent().getExtras().getString("ID");

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        ivBack = findViewById(R.id.ivBack);
        swipeRefreshLayout.setOnRefreshListener(this);
        llDeal = findViewById(R.id.llDeals);
        tvRetry = findViewById(R.id.tvRetry);
        rvCastlelist = findViewById(R.id.rvCastlelist);
        layoutManager = new LinearLayoutManager(context);
        rvCastlelist.setLayoutManager(layoutManager);
        rvCastlelist.setAdapter(castleAdapter);
        rvCastlelist.setVisibility(View.VISIBLE);


        ivBack.setOnClickListener(this);
        castleAdapter.notifyDataSetChanged();
        llMenu = findViewById(R.id.llMenu);
        llDetail = findViewById(R.id.llDetail);
        llReview = findViewById(R.id.llReview);
        llScheduled = findViewById(R.id.llScheduled);
        llReview.setOnClickListener(this);
        llScheduled.setOnClickListener(this);
        llDetail.setOnClickListener(this);
        llMenu.setOnClickListener(this);
        llDeal.setOnClickListener(this);

        getTruckList();

    }


    @Override
    public void onRefresh() {
        reviewData.clear();
        castleAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
        getTruckList();
    }


    private void getTruckList() {
        initCallbackDL();
        mVolleyService = new VolleyService(resultCallback, context);
        mVolleyService.postStringRequest(context, "POST", URLS.REVIEW, getParamsDL(), SplashActivity.TOKEN);
    }

    private Map<String, String> getParamsDL() {
        JSONObject jsonStr = new JSONObject();


        try {
            jsonStr.put("truck_id", truckID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("jsonData", jsonStr.toString());
        return map;
    }

    private void initCallbackDL() {
        resultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("response_status");
                    String msg = jsonObject.getString("response_msg");

                    if (status.equalsIgnoreCase("success")) {

                        tvRetry.setVisibility(View.GONE);
                        rvCastlelist.setVisibility(View.
                                VISIBLE);
                        reviewData.clear();
                        JSONObject jsonObj = jsonObject.getJSONObject("response_data");
                        JSONArray jsonArray = jsonObj.getJSONArray("trucks_reviews");

                        if (jsonArray.length() == 0) {
                            //tvRetry.setVisibility(View.VISIBLE);
                            swipeRefreshLayout.setRefreshing(false);
                            castleAdapter.notifyDataSetChanged();
                            tvRetry.setVisibility(View.VISIBLE);
                            rvCastlelist.setVisibility(View.
                                    GONE);


                        } else {
                            // tvRetry.setVisibility(View.GONE);

                            for (int i = 0; i < jsonArray.length(); i++) {

                                ReviewData order;
                                order = gson.fromJson(jsonArray.getJSONObject(i).toString(), ReviewData.class);
                                reviewData.add(order);

                            }

                            //showToast("" + al_data.size());
                            swipeRefreshLayout.setRefreshing(false);
                            castleAdapter.notifyDataSetChanged();
                        }
                    } else {
                        tvRetry.setVisibility(View.VISIBLE);
                        rvCastlelist.setVisibility(View.
                                GONE);

                        Utils.dialog(context, msg);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Utils.getErrors(context, error.toString());

            }
        };
    }


    private class CastleListAdapter extends RecyclerView.Adapter<CastleListAdapter.ViewHolder> {
        @Override
        public CastleListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_review_list, parent, false);
            return new CastleListAdapter.ViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(CastleListAdapter.ViewHolder holder, final int position) {
            final ReviewData userData = reviewData.get(position);


// set the drawable as progress drawable

            holder.tvReview.setText(userData.getReview_text());

            try {


                holder.ratingBar.setRating(Float.parseFloat(userData.getRating()));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return reviewData.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvReview;
            RatingBar ratingBar;


            public ViewHolder(View itemView) {
                super(itemView);
                ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
                tvReview = (TextView) itemView.findViewById(R.id.tvReview);


            }
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.ivBack:

                finish();

                break;

            case R.id.llMenu:

                startActivity(new Intent(context, MenuListActivity.class)
                        .putExtra("ID", truckID));
                finish();

                break;
            case R.id.llReview:

                //   startActivity(new Intent(context, ReviewListActivity.class)
                //             .putExtra("ID", truckID));

                break;
            case R.id.llScheduled:

                startActivity(new Intent(context, ScheduleListActivity.class)
                        .putExtra("ID", truckID));
                finish();

                break;
            case R.id.llDetail:
                startActivity(new Intent(context, TruckDetailActivity.class)
                        .putExtra("ID", truckID));
                finish();

                break;

            case R.id.llDeals:

                startActivity(new Intent(context, DealsListActivity.class)
                        .putExtra("ID", truckID));
                finish();

                break;


        }
    }

    private void getLocation() {
        tracker = new GPSTracker(ReviewListActivity.this);
        if (tracker.canGetLocation()) {

            //  lat = 35.8533935;
            // lng = -78.7024126;
            lat = tracker.getLatitude();
            lng = tracker.getLongitude();
            if (lat != 0.0) {

                updateLocationWS();
            } else {
                Utils.showDialog(ReviewListActivity.this, "Location not found. Please enable your GPS");
            }
        } else {

        }
    }


    private void updateLocationWS() {

        initVolleyCallbackUpdate();
        mVolleyService = new VolleyService(resultCallback, context);
        mVolleyService.postStringRequest(context, "GET", URLS.UPDATE_LOCATION, getParamsLocatioin(), SplashActivity.TOKEN);
    }

    private Map<String, String> getParamsLocatioin() {

        JSONObject dataa = new JSONObject();
        try {
            dataa.putOpt("user_id", Preference.getInstance(ReviewListActivity.this).getString(Constant.USER_ID));
            dataa.putOpt("lat", "" + lat);
            dataa.putOpt("lng", "" + lng);
            dataa.putOpt("truck_id", "" + truckID);
            //  dataa.putOpt("Language", Constant.LANGUAGE);
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Map<String, String> params = new HashMap<String, String>();
        // params.put("api_key", Constant.API_KEY);
        params.put("jsonData", dataa.toString());
        return params;
    }

    void initVolleyCallbackUpdate() {
        resultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, String response) {
                Log.d(Constant.TAG, " Success " + requestType);
                try {
                    JSONObject data = new JSONObject(response);

                    JSONObject responseData;


                    if (data.getString(Constant.RESPONSE_STATUS).equalsIgnoreCase(Constant.SUCCESS)) {
                        /// showToast(data.getString("response_msg"));

                        getTruckList();
                        Utils.showDialog(ReviewListActivity.this, data.getString("response_msg"));


                    } else {
                        showToast(data.getString("response_msg"));

                    }
                } catch (JSONException e) {
                    showToast(e.getLocalizedMessage());
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(Constant.TAG, " Error " + requestType);
                showToast(error.toString());

            }
        };
    }


    private void showToast(String localizedMessage) {
        //new MyCustomeToast().showToast(this, localizedMessage);
        Utils.showDialog(context, localizedMessage);
    }


}