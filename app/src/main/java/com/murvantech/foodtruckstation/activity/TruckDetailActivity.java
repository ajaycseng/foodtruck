package com.murvantech.foodtruckstation.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.murvantech.foodtruckstation.R;
import com.murvantech.foodtruckstation.model.TruckData;
import com.murvantech.foodtruckstation.model.TruckDetailData;
import com.murvantech.foodtruckstation.model.UserData;
import com.murvantech.foodtruckstation.networkcall.IResult;
import com.murvantech.foodtruckstation.networkcall.URLS;
import com.murvantech.foodtruckstation.networkcall.VolleyService;
import com.murvantech.foodtruckstation.roomdb.DatabaseClient;
import com.murvantech.foodtruckstation.roomdb.Task;
import com.murvantech.foodtruckstation.utils.Constant;
import com.murvantech.foodtruckstation.utils.MyViewPager;
import com.murvantech.foodtruckstation.utils.Preference;
import com.murvantech.foodtruckstation.utils.Utils;
import com.squareup.picasso.Picasso;
import com.testfairy.TestFairy;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TruckDetailActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    MyViewPager vpImage;
    TruckDetailData store;
    CustomPagerAdapter pagerAdapter;
    private IResult resultCallback;
    private VolleyService mVolleyService;
    Context context;
    private String truckID = "1";
    private Gson gson = new Gson();
    ImageView ivBack;
    RatingBar ratingBar;
    private TextView tvName, tvScheduled, tvReview, tvMenu, tvCategory, tvPhone, tvWebsite, tvDescription, tvAddress, tvOwnerName, tvOwnerPhone, tvEmail;


    LinearLayout llMenu, llDetail, llReview, llScheduled, llDeals, llMap;
    private GoogleMap mMap;
    private ImageView ivFav, ivReportLocation;
    Set<String> set;
    private double lat = 0.0;
    private double lng = 0.0;
    List<Task> tasks = new ArrayList<>();
    boolean isFavorite = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TestFairy.begin(this, Constant.TEST_FAIRY_TOKEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_truck_detail);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        llMap = findViewById(R.id.llMap);
        ivFav = findViewById(R.id.ivFav);
        ivReportLocation = findViewById(R.id.ivReportLocation);

        llMenu = findViewById(R.id.llMenu);
        llDetail = findViewById(R.id.llDetail);
        llReview = findViewById(R.id.llReview);
        llScheduled = findViewById(R.id.llScheduled);
        llDeals = findViewById(R.id.llDeals);
        llReview.setOnClickListener(this);
        llScheduled.setOnClickListener(this);
        llDetail.setOnClickListener(this);
        llMenu.setOnClickListener(this);
        llDeals.setOnClickListener(this);
        ivFav.setOnClickListener(this);
        ivReportLocation.setOnClickListener(this);

        tvName = findViewById(R.id.tvName);
        tvMenu = findViewById(R.id.tvMenu);
        tvReview = findViewById(R.id.tvReview);
        tvScheduled = findViewById(R.id.tvScheduled);
        ratingBar = findViewById(R.id.ratingBar);
        tvCategory = findViewById(R.id.tvCategory);
        tvPhone = findViewById(R.id.tvPhone);
        tvWebsite = findViewById(R.id.tvWebsite);
        tvDescription = findViewById(R.id.tvDescription);
        tvOwnerName = findViewById(R.id.tvOwnerName);
        tvOwnerPhone = findViewById(R.id.tvOwnerPhone);
        tvEmail = findViewById(R.id.tvEmail);
        tvAddress = findViewById(R.id.tvAddress);


        tvPhone.setOnClickListener(this);
        tvWebsite.setOnClickListener(this);
        tvOwnerPhone.setOnClickListener(this);
        tvScheduled.setOnClickListener(this);
        tvReview.setOnClickListener(this);
        tvMenu.setOnClickListener(this);

        vpImage = findViewById(R.id.vpImage);
        context = this;
        truckID = getIntent().getExtras().getString("ID");
        getDetailsWS();
        findViewById(R.id.ivBack).setOnClickListener(this);
        findViewById(R.id.ivShare).setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivFav:
                try {
                    saveTask(isFavorite);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case R.id.llMenu:

                startActivity(new Intent(context, MenuListActivity.class)
                        .putExtra("ID", truckID));
                finish();
                break;
            case R.id.llReview:

                startActivity(new Intent(context, ReviewListActivity.class)
                        .putExtra("ID", truckID));
                finish();

                break;
            case R.id.llScheduled:

                startActivity(new Intent(context, ScheduleListActivity.class)
                        .putExtra("ID", truckID));
                finish();

                break;
            case R.id.llDeals:

                startActivity(new Intent(context, DealsListActivity.class)
                        .putExtra("ID", truckID));
                finish();

                break;
            case R.id.llDetail:
                //  startActivity(new Intent(context, ScheduleListActivity.class)
                //          .putExtra("ID", truckID));


                break;

            case R.id.ivReportLocation:

                store.setLat(lat);
                store.setLng(lng);


                startActivity(new Intent(context, ReportActivity.class)
                        .putExtra("DATA", store));
                break;


            case R.id.ivShare:

/*

                yes you can ... you just need to know the exact package name of the application:

                Facebook - "com.facebook.katana"
                Twitter - "com.twitter.android"
                Instagram - "com.instagram.android"
                Pinterest - "com.pinterest"

                And you can create the intent like this

                Intent intent = context.getPackageManager().getLaunchIntentForPackage(application);
                if (intent != null) {
                    // The application exists
                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.setPackage(application);

                    shareIntent.putExtra(android.content.Intent.EXTRA_TITLE, title);
                    shareIntent.putExtra(Intent.EXTRA_TEXT, description);
                    // Start the specific social application
                    context.startActivity(shareIntent);
                } else {
                    // The application does not exist
                    // Open GooglePlay or use the default system picker
                }


*/


                String message = "Amazing food truck " + store.getName() +
                        " found at " + store.address + " " + store.city + " Download app from google play soon";
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, message);

                startActivity(Intent.createChooser(share, "Share truck detail"));
                break;

            case R.id.ivBack:

                finish();
                break;

            case R.id.tvOwnerPhone:

                if (!store.getTruck_owner_phone().equals("")) {
                    try {
                        Intent sIntent = new Intent(Intent.ACTION_CALL, Uri
                                .parse("tel:" + store.getTruck_owner_phone()));
                        sIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(sIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.tvPhone:

                if (!store.getPhone().equals("")) {
                    try {
                        Intent sIntent = new Intent(Intent.ACTION_CALL, Uri
                                .parse("tel:" + store.getPhone()));
                        sIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(sIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.tvWebsite:

                if (!store.getWebsite().equals("")) {
                    try {

                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(store.getWebsite()));
                        startActivity(i);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;


        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (googleMap != null) {
            mMap = googleMap;
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setRotateGesturesEnabled(false);
            mMap.getUiSettings().setScrollGesturesEnabled(false);
            mMap.getUiSettings().setTiltGesturesEnabled(false);
        }
    }


    class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;

        public CustomPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {

            if (store.getTruck_images() != null) {

                if (store.getTruck_images().size() == 0)
                    return 1;
                else
                    return store.getTruck_images().size();
            } else {
                return 1;
            }
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((RelativeLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
            if (store.getTruck_images() != null) {
                if (store.getTruck_images().size() == 0) {
                } else {
                    Picasso.with(mContext).load(store.getTruck_images().get(position)).fit().error(R.drawable.logo).into(imageView);
                }
            }
            container.addView(itemView);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //   startActivity(new Intent(StoreDetailActivity.this, ViewImageActivity.class).putExtra("store", store).putExtra("position", position));
                }
            });

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout) object);
        }
    }


    private void getDetailsWS() {

        initVolleyCallbackUpdate();
        mVolleyService = new VolleyService(resultCallback, context);
        mVolleyService.postStringRequest(context, "GET", URLS.TRUCK_DETAIL, getParamsLocatioin(), SplashActivity.TOKEN);
    }

    private void getLocationWS() {

        initVolleyCallbackUpdateLocation();
        mVolleyService = new VolleyService(resultCallback, context);
        mVolleyService.postStringRequest(context, "GET", URLS.TRUCK_LOCATION, getParamsLocatioin(), SplashActivity.TOKEN);
    }

    private Map<String, String> getParamsLocatioin() {

        JSONObject dataa = new JSONObject();
        try {

            dataa.putOpt("truck_id", "" + truckID);
            //  dataa.putOpt("Language", Constant.LANGUAGE);
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Map<String, String> params = new HashMap<String, String>();
        // params.put("api_key", Constant.API_KEY);
        params.put("jsonData", dataa.toString());
        return params;
    }

    void initVolleyCallbackUpdate() {
        resultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, String response) {
                Log.d(Constant.TAG, " Success " + requestType);
                try {
                    JSONObject data = new JSONObject(response);


                    if (data.getString(Constant.RESPONSE_STATUS).equalsIgnoreCase(Constant.SUCCESS)) {
                        /// showToast(data.getString("response_msg"));
                        JSONObject responseData = data.getJSONObject("response_data");
                        JSONObject truckDetail = responseData.getJSONObject("trucks_details");

                        store = gson.fromJson(truckDetail.toString(), TruckDetailData.class);
                        pagerAdapter = new CustomPagerAdapter(TruckDetailActivity.this);
                        vpImage.setAdapter(pagerAdapter);
                        pagerAdapter.notifyDataSetChanged();

                        tvName.setText(store.getName());
                        tvCategory.setText(store.getCategory());
                        tvPhone.setText(store.getPhone());
                        tvWebsite.setText(store.getWebsite());
                        tvDescription.setText(store.getDescription());
                        tvOwnerName.setText(store.getTruck_owner_name());
                        tvOwnerPhone.setText(store.getTruck_owner_phone());
                        tvEmail.setText(store.getTruck_owner_email());
                        tvAddress.setText(store.getAddress());

                        ratingBar.setRating(Float.parseFloat(store.getAverage_rating()));


                        getLocationWS();

                        getFav();

                    } else {
                        showToast(data.getString("response_msg"));

                    }
                } catch (JSONException e) {
                    showToast(e.getLocalizedMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(Constant.TAG, " Error " + requestType);
                showToast(error.toString());

            }
        };
    }

    void initVolleyCallbackUpdateLocation() {
        resultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, String response) {
                Log.d(Constant.TAG, " Success " + requestType);
                try {
                    JSONObject data = new JSONObject(response);


                    if (data.getString(Constant.RESPONSE_STATUS).equalsIgnoreCase(Constant.SUCCESS)) {
                        /// showToast(data.getString("response_msg"));
                        JSONObject responseData = data.getJSONObject("response_data");
                        JSONObject truckDetail = responseData.getJSONObject("truck_live_location");


                        try {

                            lat = Double.parseDouble(truckDetail.getString("latitude"));
                            lng = Double.parseDouble(truckDetail.getString("longitude"));
                            String name = (truckDetail.getString("loc_details"));
                            setUpMap();

                        } catch (Exception e) {
                            llMap.setVisibility(View.GONE);
                            e.printStackTrace();
                        }

                        //   setUpMap(store);


                    } else {
                        showToast(data.getString("response_msg"));

                    }
                } catch (JSONException e) {
                    showToast(e.getLocalizedMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(Constant.TAG, " Error " + requestType);
                showToast(error.toString());

            }
        };
    }

    private void setUpMap() {

        if (mMap != null) {

            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(lat, lng))
                    .title(store.name)
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.ic_marker)));
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 17);
            mMap.animateCamera(cameraUpdate);
            llMap.setVisibility(View.VISIBLE);

        }
    }


    private void showToast(String localizedMessage) {
        //new MyCustomeToast().showToast(this, localizedMessage);
        Utils.showDialog(context, localizedMessage);
    }


    // save room data

    private void saveTask(final boolean save) {


        class SaveTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                //creating a task
                Task task = new Task();


                task.setId(store.getId());


                if (store.getTruck_images().size() > 0) {
                    task.setImage(store.getTruck_images().get(0));
                }
                task.setName(store.getName());
                task.setCategory(store.getCategory());
                task.setDescription(store.getDescription());
                task.setWebsite(store.getWebsite());
                task.setPhone(store.getPhone());
                task.setEmail(store.getEmail());
                task.setAddress(store.getAddress());
                task.setCity(store.getCity());
                task.setState(store.getState());
                task.setFacebook(store.getFacebook());
                task.setTwitter(store.getTwitter());
                task.setInstagram(store.getInstagram());
                task.setGoogleplus(store.getGoogleplus());
                task.setTruck_owner_name(store.getTruck_owner_name());
                task.setTruck_owner_phone(store.getTruck_owner_phone());
                task.setAverage_rating(store.getAverage_rating());
                task.setTruck_owner_email(store.getTruck_owner_email());

                //adding to database


                if (!save) {
                    DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                            .taskDao()
                            .insert(task);
                } else {
                    DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                            .taskDao()
                            .delete(task);
                }

                return null;

            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);


                if (!isFavorite) {
                    isFavorite = true;
                    ivFav.setImageResource(R.drawable.ic_like);
                    // Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_LONG).show();
                } else {
                    isFavorite = false;
                    ivFav.setImageResource(R.drawable.ic_unlike);
                    // Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_LONG).show();
                }


            }
        }

        SaveTask st = new SaveTask();
        st.execute();
    }


    private void getFav() {
        class GetTasks extends AsyncTask<Void, Void, List<Task>> {

            @Override
            protected List<Task> doInBackground(Void... voids) {
                tasks = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .getAll();
                return tasks;
            }

            @Override
            protected void onPostExecute(List<Task> tasks) {
                super.onPostExecute(tasks);

                if (tasks.size() == 0) {
                    return;
                } else {
                    for (Task task : tasks) {

                        if (task.getId().equals(store.getId())) {
                            isFavorite = true;
                            ivFav.setImageResource(R.drawable.ic_like);
                            return;
                        }
                    }
                }


            }
        }

        GetTasks gt = new GetTasks();
        gt.execute();
    }


}
