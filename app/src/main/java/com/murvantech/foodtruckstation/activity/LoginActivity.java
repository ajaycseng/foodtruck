package com.murvantech.foodtruckstation.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.murvantech.foodtruckstation.R;
import com.murvantech.foodtruckstation.model.UserData;
import com.murvantech.foodtruckstation.networkcall.IResult;
import com.murvantech.foodtruckstation.networkcall.URLS;
import com.murvantech.foodtruckstation.networkcall.VolleyService;
import com.murvantech.foodtruckstation.utils.Constant;
import com.murvantech.foodtruckstation.utils.HideKeyboard;
import com.murvantech.foodtruckstation.utils.Preference;
import com.murvantech.foodtruckstation.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tvForgotPassword, tvSignUp;
    Button btn_signin;
    EditText etEmail, etPassword;
    String email = "", password = "";

    private IResult resultCallback;
    private VolleyService mVolleyService;
    private Context context;
    private String deviceID = "segd";
    private Gson gson = new Gson();

    ImageView ivEye, ivBack;
    private static final int RC_SIGN_IN = 999;
    private boolean isVisible = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        new HideKeyboard().setupUI(findViewById(R.id.ll), this);

        context = this;
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        ivEye = findViewById(R.id.ivEye);
        tvSignUp = findViewById(R.id.tvSignUp);
        btn_signin = findViewById(R.id.btn_signin);
        etPassword = findViewById(R.id.etPassword);
        etEmail = findViewById(R.id.etEmail);
        ivBack = findViewById(R.id.ivBack);
        tvForgotPassword.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);
        btn_signin.setOnClickListener(this);
        ivEye.setOnClickListener(this);
        ivBack.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:

                finish();
                //  startActivity(new Intent(getApplicationContext(), ForgotpasswordActivity.class));
                break;
            case R.id.tvForgotPassword:
                //  startActivity(new Intent(getApplicationContext(), ForgotpasswordActivity.class));
                break;
            case R.id.tvSignUp:
                // startActivity(new Intent(getApplicationContext(), RegistrationActivity.class));
                break;

            case R.id.btn_signin:
                validate();
                break;
            case R.id.ivEye:


                if (isVisible == false) {
                    isVisible = true;
                    etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    ivEye.setImageResource(R.drawable.ic_hide_pwd);
                } else {
                    isVisible = false;
                    ivEye.setImageResource(R.drawable.ic_show_pwd);
                    etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    // etNewPass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);

                }


                break;
        }

    }

    private void validate() {
        email = etEmail.getText().toString().trim();
        password = etPassword.getText().toString().trim();
        if (email.equals("")) {
            Utils.showDialog(context, "Please enter email address.");
        } else if (!Utils.isValidEmail(email)) {
            Utils.showDialog(context, "Please enter currect email address.");
        } else if (password.equals("")) {
            Utils.showDialog(context, "Please enter password.");
        } else {

            // QbLogin();
            callWS();
        }

    }


    private void callWS() {
        initCallback();
        mVolleyService = new VolleyService(resultCallback, this);
        mVolleyService.postStringRequest(this, "POST", URLS.LOGIN, getParamsLogin(), "");
    }


    private Map<String, String> getParamsLogin() {
        JSONObject jsonStr = new JSONObject();
        try {
            jsonStr.put("email_address", email);
            jsonStr.put("password", password);
            jsonStr.put("device_type", Constant.DEVICE_TYPE);
            jsonStr.put("device_id", "food123");//FirebaseInstanceId.getInstance().getToken());

            //jsonStr.put("action", action);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<>();
        map.put("jsonData", jsonStr.toString());
        return map;
    }

    private void initCallback() {
        resultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("response_status");
                    String msg = jsonObject.getString("response_msg");

                    if (status.equalsIgnoreCase("success")) {

                        JSONObject responseData = jsonObject.getJSONObject("response_data");

                        UserData user;
                        user = gson.fromJson(responseData.toString(), UserData.class);
                        Preference.getInstance(LoginActivity.this).putString(Constant.USER_DETAIL, responseData.toString());
                        Preference.getInstance(LoginActivity.this).putString(Constant.USER_ID, user.getUser_id());
                        // Preference.getInstance(LoginActivity.this).putString(Constant.TOKEN, user.getId());


                        //  userName = user.getUsername();
                        /// HomeActivity.TOKEN = user.getToken();


                        HomeActivity.page = "MyTrucks";
                        startActivity(new Intent(LoginActivity.this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));


                        finish();

                    } else {
                        Utils.dialog(context, msg);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                // showToast(error.toString());
                Utils.getErrors(context, error.toString());

            }
        };
    }

    private void showToast(String msg) {

        Utils.showDialog(context, msg);
    }


}
