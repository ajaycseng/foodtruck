package com.murvantech.foodtruckstation.model;

import java.io.Serializable;

/**
 * Created by lscp on 18/4/18.
 */

public class ScheduledData implements Serializable {

    public String day, schedule_day, schedule_time_from, schedule_time_to, schedule_street_address, schedule_city, schedule_state, schedule_zip;


    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getSchedule_day() {
        return schedule_day;
    }

    public void setSchedule_day(String schedule_day) {
        this.schedule_day = schedule_day;
    }

    public String getSchedule_time_from() {
        return schedule_time_from;
    }

    public void setSchedule_time_from(String schedule_time_from) {
        this.schedule_time_from = schedule_time_from;
    }

    public String getSchedule_time_to() {
        return schedule_time_to;
    }

    public void setSchedule_time_to(String schedule_time_to) {
        this.schedule_time_to = schedule_time_to;
    }

    public String getSchedule_street_address() {
        return schedule_street_address;
    }

    public void setSchedule_street_address(String schedule_street_address) {
        this.schedule_street_address = schedule_street_address;
    }

    public String getSchedule_city() {
        return schedule_city;
    }

    public void setSchedule_city(String schedule_city) {
        this.schedule_city = schedule_city;
    }

    public String getSchedule_state() {
        return schedule_state;
    }

    public void setSchedule_state(String schedule_state) {
        this.schedule_state = schedule_state;
    }

    public String getSchedule_zip() {
        return schedule_zip;
    }

    public void setSchedule_zip(String schedule_zip) {
        this.schedule_zip = schedule_zip;
    }
}