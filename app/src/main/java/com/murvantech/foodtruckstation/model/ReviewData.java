package com.murvantech.foodtruckstation.model;

import java.io.Serializable;

/**
 * Created by lscp on 18/4/18.
 */

public class ReviewData implements Serializable {

    private String rating, review_text, deal_title, deal_price_strike, deal_price, deal_end_date;


    public String getDeal_title() {
        return deal_title;
    }

    public void setDeal_title(String deal_title) {
        this.deal_title = deal_title;
    }

    public String getDeal_price_strike() {
        return deal_price_strike;
    }

    public void setDeal_price_strike(String deal_price_strike) {
        this.deal_price_strike = deal_price_strike;
    }

    public String getDeal_price() {
        return deal_price;
    }

    public void setDeal_price(String deal_price) {
        this.deal_price = deal_price;
    }

    public String getDeal_end_date() {
        return deal_end_date;
    }

    public void setDeal_end_date(String deal_end_date) {
        this.deal_end_date = deal_end_date;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReview_text() {
        return review_text;
    }

    public void setReview_text(String review_text) {
        this.review_text = review_text;
    }
}