package com.murvantech.foodtruckstation.model;

import java.io.Serializable;

/**
 * Created by lscp on 18/4/18.
 */

public class CategoryData implements Serializable {

    private String id, name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}