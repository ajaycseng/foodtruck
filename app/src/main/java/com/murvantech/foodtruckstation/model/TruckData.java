package com.murvantech.foodtruckstation.model;


import java.io.Serializable;

/**
 * Created by lscp on 18/4/18.
 */

public class TruckData implements Serializable {

    private String id;
    private String user_id;
    private String name;
    private String description;
    private String truck_image;
    private String phone;
    private String truck_lat;
    private String truck_long;
    private String distance;
    private String latitude;
    private String longitude;
    private String address_location;


    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAddress_location() {
        return address_location;
    }

    public void setAddress_location(String address_location) {
        this.address_location = address_location;
    }

    public String getTruck_lat() {
        return truck_lat;
    }

    public void setTruck_lat(String truck_lat) {
        this.truck_lat = truck_lat;
    }

    public String getTruck_long() {
        return truck_long;
    }

    public void setTruck_long(String truck_long) {
        this.truck_long = truck_long;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTruck_image() {
        return truck_image;
    }

    public void setTruck_image(String truck_image) {
        this.truck_image = truck_image;
    }
}
