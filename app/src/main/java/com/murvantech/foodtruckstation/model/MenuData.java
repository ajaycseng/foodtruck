package com.murvantech.foodtruckstation.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by lscp on 18/4/18.
 */

public class MenuData implements Serializable {

    private String menu_category_name;
    ArrayList<MenuCategoryItem> menu_category_items = new ArrayList<>();


    public ArrayList<MenuCategoryItem> getMenu_category_items() {
        return menu_category_items;
    }

    public void setMenu_category_items(ArrayList<MenuCategoryItem> menu_category_items) {
        this.menu_category_items = menu_category_items;
    }

    public String getMenu_category_name() {
        return menu_category_name;
    }

    public void setMenu_category_name(String menu_category_name) {
        this.menu_category_name = menu_category_name;
    }
}