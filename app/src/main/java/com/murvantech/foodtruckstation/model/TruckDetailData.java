package com.murvantech.foodtruckstation.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;

@Entity
public class TruckDetailData implements Serializable {

    @NonNull
    @PrimaryKey(autoGenerate = false)
    public String id="";

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "category")
    public String category;

    @ColumnInfo(name = "description")
    public String description;

    @ColumnInfo(name = "website")
    public String website;

    @ColumnInfo(name = "phone")
    public String phone;

    @ColumnInfo(name = "email")
    public String email;

    @ColumnInfo(name = "address")
    public String address;

    @ColumnInfo(name = "city")
    public String city;

    @ColumnInfo(name = "state")
    public String state;

    @ColumnInfo(name = "facebook")
    public String facebook;

    @ColumnInfo(name = "twitter")
    public String twitter;

    @ColumnInfo(name = "instagram")
    public String instagram;

    @ColumnInfo(name = "googleplus")
    public String googleplus;

    @ColumnInfo(name = "truck_owner_name")
    public String truck_owner_name;

    @ColumnInfo(name = "truck_owner_email")
    public String truck_owner_email;

    @ColumnInfo(name = "truck_owner_phone")
    public String truck_owner_phone;

    @ColumnInfo(name = "average_rating")
    public String average_rating;

    @ColumnInfo(name = "lat")
    public Double lat = 0.0;

    @ColumnInfo(name = "lnt")
    public Double lng = 0.0;


    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
    @ColumnInfo(name = "truck_images")
    public ArrayList<String> truck_images = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getGoogleplus() {
        return googleplus;
    }

    public void setGoogleplus(String googleplus) {
        this.googleplus = googleplus;
    }

    public String getTruck_owner_name() {
        return truck_owner_name;
    }

    public void setTruck_owner_name(String truck_owner_name) {
        this.truck_owner_name = truck_owner_name;
    }

    public String getTruck_owner_email() {
        return truck_owner_email;
    }

    public void setTruck_owner_email(String truck_owner_email) {
        this.truck_owner_email = truck_owner_email;
    }

    public String getTruck_owner_phone() {
        return truck_owner_phone;
    }

    public void setTruck_owner_phone(String truck_owner_phone) {
        this.truck_owner_phone = truck_owner_phone;
    }

    public String getAverage_rating() {
        return average_rating;
    }

    public void setAverage_rating(String average_rating) {
        this.average_rating = average_rating;
    }

    public ArrayList<String> getTruck_images() {
        return truck_images;
    }

    public void setTruck_images(ArrayList<String> truck_images) {
        this.truck_images = truck_images;
    }
}
