package com.murvantech.foodtruckstation.networkcall;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.murvantech.foodtruckstation.R;
import com.murvantech.foodtruckstation.utils.Utils;

import java.util.HashMap;
import java.util.Map;


public class VolleyService {

    IResult mResultCallback = null;
    Context mContext;

    Map<String, String> paramsdata = new HashMap<String, String>();


    public VolleyService(IResult resultCallback, Context context) {
        mResultCallback = resultCallback;
        mContext = context;
    }

    public void postStringRequest(final Context context, final String requestType, String url, final Map<String, String> params, final String token) {
        final ProgressDialog pDialog = Utils.progressDialog(context);
        pDialog.show();
        paramsdata = params;
        mContext = context;
        try {
            if (!Utils.isInternetConnected(context)) {
                pDialog.hide();
                Utils.showDialog(context, context.getResources().getString(R.string.errorInternet));
                return;
            } else {
                RequestQueue queue = Volley.newRequestQueue(mContext);
                StringRequest strReq = new StringRequest(Request.Method.POST,
                        url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (mResultCallback != null)
                            mResultCallback.notifySuccess(requestType, response);
                        pDialog.hide();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mResultCallback != null)
                            mResultCallback.notifyError(requestType, error);
                        pDialog.hide();
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {

                        final Map<String, String> headers = new HashMap<>();
                        headers.put("key", "foodT00985TruckYGNLya");
                       // headers.put("Content-Type", "application/x-www-form-urlencoded");

                      /*  if (!token.equals("")) {
                            headers.put("token", token);
                        }*/
                        return headers;
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        return paramsdata;
                    }

                };

                strReq.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(strReq);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void postStringRequestContentType(final Context context, final String requestType, String url, final Map<String, String> params, final String token) {
        final ProgressDialog pDialog = Utils.progressDialog(context);
        pDialog.show();
        paramsdata = params;
        mContext = context;
        try {
            if (!Utils.isInternetConnected(context)) {
                pDialog.hide();
                Utils.showDialog(context, context.getResources().getString(R.string.errorInternet));
                return;
            } else {
                RequestQueue queue = Volley.newRequestQueue(mContext);
                StringRequest strReq = new StringRequest(Request.Method.POST,
                        url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (mResultCallback != null)
                            mResultCallback.notifySuccess(requestType, response);
                        pDialog.hide();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mResultCallback != null)
                            mResultCallback.notifyError(requestType, error);
                        pDialog.hide();
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {

                        final Map<String, String> headers = new HashMap<>();
                        headers.put("key", "foodT00985TruckYGNLya");
                        headers.put("Content-Type", "application/x-www-form-urlencoded");

                      /*  if (!token.equals("")) {
                            headers.put("token", token);
                        }*/
                        return headers;
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        return paramsdata;
                    }

                };

                strReq.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(strReq);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getStringRequest(final Context context, final String requestType, String url, final Map<String, String> params, final String token) {
        final ProgressDialog pDialog = Utils.progressDialog(context);
        pDialog.show();
        paramsdata = params;
        mContext = context;
        try {
            if (!Utils.isInternetConnected(context)) {
                pDialog.hide();
                Utils.showDialog(context, context.getResources().getString(R.string.errorInternet));
                return;
            } else {
                RequestQueue queue = Volley.newRequestQueue(mContext);
                StringRequest strReq = new StringRequest(Request.Method.GET,
                        url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (mResultCallback != null)
                            mResultCallback.notifySuccess(requestType, response);
                        pDialog.hide();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mResultCallback != null)
                            mResultCallback.notifyError(requestType, error);
                        pDialog.hide();
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {

                        final Map<String, String> headers = new HashMap<>();
                        headers.put("key", "foodT00985TruckYGNLya");

                       /* if (!token.equals("")) {
                            headers.put("token", token);
                        }*/
                        return headers;
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        return paramsdata;
                    }

                };
                strReq.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(strReq);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void sendImgRequest(final Context context, final String requestType, final Bitmap bitmap, String url, final Map<String, String> params, final String token) {
        final ProgressDialog pDialog = Utils.progressDialog(context);
        pDialog.show();
        paramsdata = params;
        mContext = context;


        try {
            if (!Utils.isInternetConnected(context)) {
                pDialog.hide();
                Utils.showDialog(context, context.getResources().getString(R.string.errorInternet));
                return;
            } else {
                RequestQueue queue = Volley.newRequestQueue(mContext);


                StringRequest strReq = new StringRequest(Request.Method.GET,
                        url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (mResultCallback != null)
                            mResultCallback.notifySuccess(requestType, response);
                        pDialog.hide();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mResultCallback != null)
                            mResultCallback.notifyError(requestType, error);
                        pDialog.hide();
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {

                        final Map<String, String> headers = new HashMap<>();
                        headers.put("key", "foodT00985TruckYGNLya");

                        if (!token.equals("")) {
                            headers.put("token", token);
                        }
                        return headers;
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        return paramsdata;
                    }

                };

                strReq.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(strReq);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void postStringRequestinbg(final Context context, final String requestType, String url, Map<String, String> params) {
        //  final ProgressDialog pDialog = Utils.progressDialog(context);
        // pDialog.show();
        paramsdata = params;
        mContext = context;
        try {
            RequestQueue queue = Volley.newRequestQueue(mContext);
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (mResultCallback != null)
                        mResultCallback.notifySuccess(requestType, response);
                    // pDialog.hide();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (mResultCallback != null)
                        mResultCallback.notifyError(requestType, error);
                    // pDialog.hide();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return paramsdata;
                }
            };
            queue.add(strReq);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}