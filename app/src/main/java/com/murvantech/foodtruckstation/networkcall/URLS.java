package com.murvantech.foodtruckstation.networkcall;


public interface URLS {

// BWv6qcLhDbxjrxMTH3Z2
    // Live URL


    //  String BASE_URL = "http://foodtruckstationclt.com/api/";
    // String TERMS = "http://foodtruckstationclt.com/pages/terms";
    // String IMG_BASE_URL = "http://darebux.logicspice.com/api/";


    String BASE_URL = "http://demo.bestwebsiteapps.com/food_truck_station/api/";
    String TERMS = "http://foodtruckstationclt.com/pages/terms";

    // Demo URL
    // String BASE_URL = "http://demo.voizacinc.com/food_truck_station/api/";
    // String TERMS = "http://demo.voizacinc.com/food_truck_station/pages/terms";


    // String IMG_BASE_URL = "http://darebux.logicspice.com/api/";


    // Profile Image
    String IMG_BASE_URL = "";


    String CATEGORY_LIST = BASE_URL + "categories/listing";
    String TRUCK_LIST = BASE_URL + "trucks/listing";
    String LOGIN = BASE_URL + "users/login";
    String MY_TRUCKS = BASE_URL + "trucks/mytrucks";
    String HOT_SPOT = BASE_URL + "lots/nearby";
    String DEALS = BASE_URL + "trucks/deals";


    String REVIEW = BASE_URL + "trucks/reviews";
    String MENU = BASE_URL + "trucks/menu";
    String SECHEDULED = BASE_URL + "trucks/schedule";
    String SEARCH = BASE_URL + "trucks/search";
    // String UPDATE_MY_TRUCKS = BASE_URL + "trucks/updatelocation";
    String TRUCK_DETAIL = BASE_URL + "trucks/details";
    String LOT_DETAIL = BASE_URL + "lots/details";

    String TRUCK_LOCATION = BASE_URL + "trucks/livelocation";
    String REPORT_LOCATION = BASE_URL + "trucks/reportlocation";



    String UPDATE_LOCATION = BASE_URL + "trucks/updatelocation";
}
