package com.murvantech.foodtruckstation.utils;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.google.android.gms.maps.model.LatLng;
import com.murvantech.foodtruckstation.R;
import com.murvantech.foodtruckstation.networkcall.Errors;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.WIFI_SERVICE;


public class Utils {
    static Dialog dialog;
    private static GPSTracker gps;

    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidPasswordPattern(final String password) {
        Pattern pattern;
        Matcher matcher;
        // For Special Charactor included
        final String PASSWORD_PATTERN =
                "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
        // final String PASSWORD_PATTERN =
        // "^(?=.*[0-9])(?=.*[A-Za-z])(?=\\S+$).{6,}$";

        // final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,})";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        boolean matches = matcher.matches();
        System.out.println("Utils.isValidPasswordPattern()" + matches);
        return matches;

    }


    public static String forTwoDecimal(String value) {
        double result = Double.parseDouble(value);

        Double d = result;
        DecimalFormat df = new DecimalFormat("00.00");

        return "" + df.format(result);

    }

    public static String forOneDecimal(String value) {
        double result = Double.parseDouble(value);
        DecimalFormat df = new DecimalFormat("0.0");

        return "" + df.format(result);

    }


    public static String getRandonNumber() {

        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }

        return sb.toString();
    }


    public static String getLocalIpAddress(Context context) {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("IP address", "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("IP address", ex.toString());
        }

        WifiManager wm = (WifiManager) context.getSystemService(WIFI_SERVICE);
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        return "" + ip;
    }

  /*  public static String getAddedDaysInDate(int days) {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => "+c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        // formattedDate have current date/time

        String dt = formattedDate;  // Start date
        SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(dt));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, days);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
        SimpleDateFormat sdf1 = new SimpleDateFormat("MM-dd-yyyy");
        String output = sdf1.format(c.getTime());
    }*/

    public static ProgressDialog progressDialog(Context mContext) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        try {
            dialog.show();

            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.custome_progress_dialog);

/*
            GifMovieView image = (GifMovieView) dialog.findViewById(R.id.iv);
            // GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(image);
            // Glide.with(mContext).load(R.drawable.loader).into(imageViewTarget);

            InputStream stream = null;
            try {
                stream = mContext.getAssets().open("loader.gif");
                image = new GifMovieView(mContext, stream);

            } catch (IOException e) {
                e.printStackTrace();
            }*/
            //GifWebView view = new GifWebView(mContext, "file:///android_asset    /loader.gif");

        } catch (Exception e) {

        }
        return dialog;
    }

    public static Date getDateFromDatePicker(DatePickerDialog datePicker) {
        int day = datePicker.getDatePicker().getDayOfMonth();
        int month = datePicker.getDatePicker().getMonth();
        int year = datePicker.getDatePicker().getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTime();
    }

    public static void noInternetDialog(final Context context) {
        dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // dialog.setTitle("Please select an Event !");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        int screenWidth = display.getWidth();
        // int screenHeight = display.getHeight();

        dialog.setContentView(R.layout.dialog_no_internet);

        TextView btnRetry = (TextView) dialog.findViewById(R.id.btnRetry);
        ImageView image = (ImageView) dialog.findViewById(R.id.iv);
        ImageView ivClose = (ImageView) dialog.findViewById(R.id.ivClose);

        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(image);
        Glide.with(context).load(R.drawable.logo).into(imageViewTarget);

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isInternetConnected(context)) {
                    dialog.dismiss();
                }


            }
        });
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setLayout((int) (screenWidth / 1.1), LinearLayout.LayoutParams.WRAP_CONTENT);

    }

    public static boolean isInternetConnected(Context context) {
        ConnectivityManager connec = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED//
                || connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING //
                || connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING//
                || connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }

        return false;
    }

    //
//    static public void dialog(Context context, String message) {
//        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.setTitle(context.getResources().getString(R.string.app_name));
//        builder.setMessage(message);
//        builder.setPositiveButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//        AlertDialog dialog = builder.create();
//        dialog.show();
//    }
    static public void dialog(Context context, String message) {


        showDialog(context, message);

    }

    public static long compareDate(String date) {

        long retrn = 0;

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
            Date parsed = sdf.parse(date);

            Date now = new Date(System.currentTimeMillis()); // 2016-03-10 22:06:10

            System.out.println(parsed.compareTo(now));
            return parsed.compareTo(now);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return retrn;

    }

    public static LatLng GetLatLongFromAddress(Context context, String address) {

        Geocoder coder = new Geocoder(context);
        LatLng latLng = null;
        try {
            ArrayList<Address> adresses = (ArrayList<Address>) coder.getFromLocationName(address, 1);
            for (Address add : adresses) {

                {
                    latLng = new LatLng(add.getLatitude(), add.getLongitude());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return latLng;
    }


    public static long newDate(String currentDate, String expiry) {
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);// hh:mm:ss
        long diff = 0;

        try {
            Date date1 = myFormat.parse(expiry);
            Date date2 = myFormat.parse(currentDate);
            diff = date2.getTime() - date1.getTime();
            System.out.println("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public static String getCurrentDate() {


        return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH).format(Calendar.getInstance().getTime());

    }

    public static String getCurrentDate1() {


        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(Calendar.getInstance().getTime());

    }

    public static String addDaysInCurrentDate(int days) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // Now use today date.
        c.add(Calendar.DATE, days); // Adding 5 days
        String output = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH).format(c.getTime());
        System.out.println(output);

        return output;


    }

    public static String getCompleteAddressString(Context context, double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            } else {
                //  Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            //  Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }


    public static void ratingbarColor(RatingBar ratingbar) {
        LayerDrawable stars = (LayerDrawable) ratingbar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.parseColor("#FEE100"), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
    }


    static public void dialogAction(Context context, String message, final DialogListener listener) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.app_name));
        builder.setMessage(message);
        builder.setPositiveButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                listener.onClick();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showDialog(final Context context, String msg) {
        dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // dialog.setTitle("Please select an Event !");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        int screenWidth = display.getWidth();
        // int screenHeight = display.getHeight();

        dialog.setContentView(R.layout.dialog_msg);

        TextView tv_msg = (TextView) dialog.findViewById(R.id.tv_msg);
        TextView btnOK = (TextView) dialog.findViewById(R.id.btn_submit);

        tv_msg.setText(msg);

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setLayout((int) (screenWidth / 1.1), LinearLayout.LayoutParams.WRAP_CONTENT);

    }

    public static void showDialogAction(final Context context, String msg, final View.OnClickListener listener) {
        dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // dialog.setTitle("Please select an Event !");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        int screenWidth = display.getWidth();
        // int screenHeight = display.getHeight();

        dialog.setContentView(R.layout.dialog_msg);

        TextView tv_msg = (TextView) dialog.findViewById(R.id.tv_msg);
        TextView btnOK = (TextView) dialog.findViewById(R.id.btn_submit);

        tv_msg.setText(msg);

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(v);
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setLayout((int) (screenWidth / 1.1), LinearLayout.LayoutParams.WRAP_CONTENT);

    }


    /* public static LatLng GetLatLongFromAddress(Activity context, String address) {

         Geocoder coder = new Geocoder(context);
         LatLng latLng = null;
         try {
             ArrayList<Address> adresses = (ArrayList<Address>) coder.getFromLocationName(address, 1);
             for (Address add : adresses) {

                 {
                     latLng = new LatLng(add.getLatitude(), add.getLongitude());
                 }
             }
         } catch (IOException e) {
             e.printStackTrace();
         }

         return latLng;
     }

     public static LatLng getLocationFromAddress(Context context, String strAddress) {

         Geocoder coder = new Geocoder(context);
         List<Address> address;
         LatLng p1 = null;
         try {
             // May throw an IOException
             address = coder.getFromLocationName(strAddress, 5);
             if (address == null) {
                 return null;
             }
             Address location = address.get(0);
             location.getLatitude();
             location.getLongitude();

             p1 = new LatLng(location.getLatitude(), location.getLongitude());

         } catch (IOException ex) {

             ex.printStackTrace();
         }

         return p1;
     }
 */
    public static double GetDistance(Activity context, double toLat, double toLong, double fromLat, double fromLong) {

        Location locationA = new Location("point A");

        locationA.setLatitude(toLat);
        locationA.setLongitude(toLong);

        Location locationB = new Location("point B");

        locationB.setLatitude(fromLat);
        locationB.setLongitude(fromLong);

        double distance = locationA.distanceTo(locationB);


        return distance;
    }

 /*   public static  void getLocation(Context context) {
        gps = new GPSTracker(context);
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();

        // check if GPS enabled
        if (gps.canGetLocation()) {

            currentLat = latitude;
            currentLong = longitude;


            //  showToast("" + currentLong + "" + currentLat);


        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
            //  gps.showGpsONAlert();

            //   turnGPSOn();


        }

    }*/


  /*  public static void styleMap(GoogleMap googleMap) {
        String s = "[\n" + " {\n" + " \"elementType\": \"geometry\",\n" + " \"stylers\": [\n" + " {\n" + " \"color\": \"#f5f5f5\"\n" + " }\n" + " ]\n" + " },\n" + " {\n" + " \"elementType\": \"labels.icon\",\n" + " \"stylers\": [\n" + " {\n" + " \"visibility\": \"off\"\n" + " }\n" + " ]\n" + " },\n" + " {\n" + " \"elementType\": \"labels.text.fill\",\n" + " \"stylers\": [\n" + " {\n" + " \"color\": \"#616161\"\n" + " }\n" + " ]\n" + " },\n" + " {\n" + " \"elementType\": \"labels.text.stroke\",\n" + " \"stylers\": [\n" + " {\n" + " \"color\": \"#f5f5f5\"\n" + " }\n" + " ]\n" + " },\n" + " {\n" + " \"featureType\": \"administrative.land_parcel\",\n" + " \"elementType\": \"labels.text.fill\",\n" + " \"stylers\": [\n" + " {\n" + " \"color\": \"#bdbdbd\"\n" + " }\n" + " ]\n" + " },\n" + " {\n" + " \"featureType\": \"poi\",\n" + " \"elementType\": \"geometry\",\n" + " \"stylers\": [\n" + " {\n" + " \"color\": \"#eeeeee\"\n" + " }\n" + " ]\n" + " },\n" + " {\n" + " \"featureType\": \"poi\",\n" + " \"elementType\": \"labels.text.fill\",\n" + " \"stylers\": [\n" + " {\n" + " \"color\": \"#757575\"\n" + " }\n" + " ]\n" + " },\n" + " {\n" + " \"featureType\": \"poi.park\",\n" + " \"elementType\": \"geometry\",\n" + " \"stylers\": [\n" + " {\n" + " \"color\": \"#e5e5e5\"\n" + " }\n" + " ]\n" + " },\n" + " {\n" + " \"featureType\": \"poi.park\",\n" + " \"elementType\": \"labels.text.fill\",\n" + " \"stylers\": [\n" + " {\n" + " \"color\": \"#9e9e9e\"\n" + " }\n" + " ]\n" + " },\n" + " {\n" + " \"featureType\": \"road\",\n" + " \"elementType\": \"geometry\",\n" + " \"stylers\": [\n" + " {\n" + " \"color\": \"#ffffff\"\n" + " }\n" + " ]\n" + " },\n" + " {\n" + " \"featureType\": \"road.arterial\",\n" + " \"elementType\": \"labels.text.fill\",\n" + " \"stylers\": [\n" + " {\n" + " \"color\": \"#757575\"\n" + " }\n" + " ]\n" + " },\n" + " {\n" + " \"featureType\": \"road.highway\",\n" + " \"elementType\": \"geometry\",\n" + " \"stylers\": [\n" + " {\n" + " \"color\": \"#dadada\"\n" + " }\n" + " ]\n" + " },\n" + " {\n" + " \"featureType\": \"road.highway\",\n" + " \"elementType\": \"labels.text.fill\",\n" + " \"stylers\": [\n" + " {\n" + " \"color\": \"#616161\"\n" + " }\n" + " ]\n" + " },\n" + " {\n" + " \"featureType\": \"road.local\",\n" + " \"elementType\": \"labels.text.fill\",\n" + " \"stylers\": [\n" + " {\n" + " \"color\": \"#9e9e9e\"\n" + " }\n" + " ]\n" + " },\n" + " {\n" + " \"featureType\": \"transit.line\",\n" + " \"elementType\": \"geometry\",\n" + " \"stylers\": [\n" + " {\n" + " \"color\": \"#e5e5e5\"\n" + " }\n" + " ]\n" + " },\n" + " {\n" + " \"featureType\": \"transit.station\",\n" + " \"elementType\": \"geometry\",\n" + " \"stylers\": [\n" + " {\n" + " \"color\": \"#eeeeee\"\n" + " }\n" + " ]\n" + " },\n" + " {\n" + " \"featureType\": \"water\",\n" + " \"elementType\": \"geometry\",\n" + " \"stylers\": [\n" + " {\n" + " \"color\": \"#c9c9c9\"\n" + " }\n" + " ]\n" + " },\n" + " {\n" + " \"featureType\": \"water\",\n" + " \"elementType\": \"labels.text.fill\",\n" + " \"stylers\": [\n" + " {\n" + " \"color\": \"#9e9e9e\"\n" + " }\n" + " ]\n" + " }\n" + "]";
        boolean success = googleMap.setMapStyle(new MapStyleOptions(s));
        if (!success) {
            Log.e("Map", "Style parsing failed.");
        }
    }

    public static double forFourDecimalDouble(Double value) {
        double result = value;
        DecimalFormat df = new DecimalFormat("#.0000");

        return Double.valueOf("" + df.format(result));

    }*/


    public static void getErrors(Context context, String error) {
        switch (error) {
            case Errors.TIME_OUT:
                dialog(context, context.getResources().getString(R.string.errorTimeOut));
                break;
            case Errors.NO_INTERNET:
                dialog(context, context.getResources().getString(R.string.errorNoInternet));
                break;
            case Errors.SERVER_ERROR:
                dialog(context, context.getResources().getString(R.string.serverError));
                break;
            default:
                dialog(context, context.getResources().getString(R.string.serverError));
        }

    }

    public static String getFormatedAmount(double amount) {


        NumberFormat numberFormat = new DecimalFormat("#,##0.00");

        amount = (float) Math.round(amount * 100) / 100;
        return numberFormat.format(amount);

        //  return NumberFormat.getNumberInstance(Locale.US).format(amount);
    }

    public static double getETA(double dist) {


        double time = 0.0;
        try {
            double distance = dist; // kilometers
            double speed = 40.0d; // kmph

            //   double speed_in_meters_per_minute = (speed * 1000) / 60; // mpm

            time = (double) (distance / speed) * 60;


            return time;

        } catch (Exception e) {
            e.printStackTrace();

        }
        return time;

    }


    public static String getDuration(String timeTravel) {
        String duration = "";
        duration = timeTravel;

        if (!duration.equals("")) {


            long minutes = TimeUnit.MILLISECONDS.toMinutes((long) Double.parseDouble(duration));
            long hours = TimeUnit.MILLISECONDS.toHours((long) Double.parseDouble(duration));
            if (hours > 0) {
                duration += hours + "hr";
                if (minutes != 0) {
                    duration += " " + minutes + " Min";

                }

            } else {
                duration += minutes + " Min";
            }

        }

        return duration;


    }

/*    public static void showTextDetail(Activity activity, String page_description) {


        LayoutInflater inflater = LayoutInflater.from(activity);
        View view = inflater.inflate(R.layout.txtpop_up, null);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
        TextView textview = (TextView) view.findViewById(R.id.textmsg);
        textview.setText(page_description);
        alertDialog.setTitle("Description");
        alertDialog.setCancelable(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textview.setText(Html.fromHtml(page_description, Html.FROM_HTML_MODE_COMPACT));
        } else {
            textview.setText(Html.fromHtml(page_description));
        }

//alertDialog.setMessage("Here is a really long message.");
        alertDialog.setView(view);


        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        final AlertDialog alert = alertDialog.create();
        alert.show();
    }*/

    public static double forFourDecimalDouble(Double value) {
        double result = value;
        DecimalFormat df = new DecimalFormat("#.0000");

        return Double.valueOf("" + df.format(result));

    }


}





