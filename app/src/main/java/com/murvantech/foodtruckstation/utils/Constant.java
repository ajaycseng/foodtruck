package com.murvantech.foodtruckstation.utils;

public interface Constant {


    String USER_ID = "user_id";
    String TEST_FAIRY_TOKEN = "73b1eaded2e712e496532d7d2162037d25b68d8b";
    String LOGIN_DETAILS = "foodtruckstation";
    String TAG = "error";
    String RESPONSE_STATUS = "response_status";
    String RESPONSE_MSG = "response_msg";
    String RESPONSE_DATA = "response_data";
    String SUCCESS = "success";

    String DEVICE_TYPE = "Android";
    String USER_DETAIL = "userDetail";
    String FAV_LIST = "favList";
}
