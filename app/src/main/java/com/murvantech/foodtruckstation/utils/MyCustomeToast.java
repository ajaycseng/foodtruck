package com.murvantech.foodtruckstation.utils;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.murvantech.foodtruckstation.R;


public class MyCustomeToast {

    private static Toast toast;


    public void showToast(Context context, String msg) {

        cancelToast(context);

        toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);

        View view = toast.getView();
       // view.setBackgroundResource(R.drawable.bg_blue);

        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setPadding(10, 5, 10, 5);
        v.setTextColor(context.getResources().getColor(R.color.black));
        // /* here you can do anything with text */
        toast.show();
    }

    /*public void showErrorDialog(Context context, String msg) {

        Utility.dialogMessage(context, msg, context.getString(R.string.error));

    }

    public void showSuccessDialog(Context context, String msg) {

        Utility.dialogSuccess(context, msg, context.getString(R.string.success));

    }

    public void showSuccessDialogAction(Context context, String msg, String action) {

        Utility.dialogSuccess(context, msg, context.getString(R.string.success), action);

    }*/




    // private void cancelToastTimer(Context context, int time) {
    // Handler handler = new Handler();
    // handler.postDelayed(new Runnable() {
    // @Override
    // public void run() {
    // if (toast != null) {
    // toast.cancel();
    //
    // }
    // }
    // }, time);
    //
    // }

    public void cancelToast(Context context) {

        if (toast != null) {
            toast.cancel();

        }
    }
}
