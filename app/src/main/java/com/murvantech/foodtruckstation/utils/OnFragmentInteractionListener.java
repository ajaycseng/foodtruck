package com.murvantech.foodtruckstation.utils;

import android.net.Uri;
import android.support.v4.app.Fragment;

public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    void updateData();
    void replaceFragment(Fragment fragment);
    void setSelection(int position);

    }