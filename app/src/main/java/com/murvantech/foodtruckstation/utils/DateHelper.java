package com.murvantech.foodtruckstation.utils;

import android.support.v4.app.FragmentActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DateHelper {
    Date date;

    public DateHelper(Date d, FragmentActivity activity) {
        date = d;
    }

    public String getFormatteddate() {
        Date currentDate = new Date(System.currentTimeMillis());

        if (currentDate.before(date)) {


        } else if (currentDate.after(date)) {

            int diffDays = currentDate.getDate() - date.getDate();
            int diffMonth = currentDate.getMonth() - date.getMonth();
            int diffYear = currentDate.getYear() - date.getYear();

            System.out.println(diffDays);
            System.out.println(diffMonth);
            System.out.println(diffYear);

            SimpleDateFormat format;
            if (diffYear > 0) {
                //display year,month and day
                format = new SimpleDateFormat("MMM dd, yyyy");
                return format.format(date);


            } else if (diffMonth > 0) {
                //display month and day

                format = new SimpleDateFormat("MMM dd");
                return format.format(date);

            } else if (diffDays > 0 && diffDays < 8) {
                // display day name here


                switch (date.getDay() + 1) {
                    case Calendar.SUNDAY:
                        return "Sunday";

                    case Calendar.MONDAY:
                        return "Monday";

                    case Calendar.TUESDAY:
                        return "Tuesday";

                    case Calendar.WEDNESDAY:
                        return "Wednesday";

                    case Calendar.THURSDAY:
                        return "Thursday";

                    case Calendar.FRIDAY:
                        return "Friday";

                    case Calendar.SATURDAY:
                        return "Saturday";
                }
            } else if (diffDays + diffMonth + diffYear == 0) {


                return "Today";
            } else {
                format = new SimpleDateFormat("MMM dd");
                return format.format(date);

            }

        } else {
            return "today";
        }

        return "";
    }

}

