package com.murvantech.foodtruckstation.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Created by logicspice on 2/9/16.
 */
public class GetOTP extends BroadcastReceiver {
    String otp = "";

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {




            /*SmsMessage messages[]= new SmsMessage[0];
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                messages = Telephony.Sms.Intents.getMessagesFromIntent(intent);
            }
            if (messages.length>=1){
                otp =messages[0].getMessageBody().replace("Your confirmation code is","").trim();
            Intent OTP=new Intent("OTPFilter");
                OTP.putExtra("OTP_PIN",otp);
                context.sendBroadcast(OTP);



            }*/
                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();

                    Log.i("SmsReceiver", "senderNum: " + senderNum + "; message: " + message);

                    message = message.replace("Your confirmation code is", "").trim();
                    Integer.parseInt(message);

                    // TODO: 3/9/16
                    Intent otpIntent = new Intent("OTPFilter");
                    otpIntent.putExtra("OTP_PIN", message);
                    context.sendBroadcast(otpIntent);


                    // Show alert
                } // end for loop
            } //


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

