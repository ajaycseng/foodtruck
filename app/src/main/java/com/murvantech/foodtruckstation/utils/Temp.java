package com.murvantech.foodtruckstation.utils;/*
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import ls.co.satisfaid.R;
import ls.co.satisfaid.Utility.CustomAutoCompleteTextView;
import ls.co.satisfaid.Utility.GPSTracker;
import ls.co.satisfaid.Utility.HideKeyboard;
import ls.co.satisfaid.Utility.PlaceJSONParser;
import ls.co.satisfaid.Utility.ReloadFragmentInterface;
import ls.co.satisfaid.Utility.UnCaughtException;
import ls.co.satisfaid.Utility.Utils;

public class GetAddressFromMapActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback, GoogleMap.OnMapClickListener {

    private GoogleMap googleMap;
    // private GoogleMap mMap;

    private GPSTracker gps;
    private Geocoder geocoder;
    private String address = "";
    private ImageView tvBack;
    //private ImageView ivDone;
    GoogleApiClient mGoogleApiClient;
    Button ivok;
    CustomAutoCompleteTextView etStartLocation;

    TextView tvTitle;
    boolean closeEnable = false;
    private MarkerOptions markerOptions;
    private double currentLat = 0.0, currentlng = 0.0;
    private String currentAddress = "";
    private JSONObject end;
    private String googleKey;
    private ParserAutoTask parserAutoTask;
    private String placeID;
    private PlacesTask placesTask;
    private JSONArray data;
    ArrayList<HashMap<String, String>> al_data = new ArrayList<HashMap<String, String>>();
    private String locationSelected = "";
    LinearLayout llMain;
    boolean fromMarker = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(this));
        setContentView(R.layout.activity_get_address_from_map);

        new HideKeyboard().setupUI(findViewById(R.id.llMain), this);

        mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(Places.GEO_DATA_API).build();

        tvTitle = (TextView) findViewById(R.id.tvTitle);
//        tvTitle.setTypeface(Typeface.createFromAsset(getAssets(), "CaviarDreams.ttf"));

        Intent postFavor = getIntent();


        if (postFavor != null) {
            currentAddress = postFavor.getStringExtra("address");
            currentLat = postFavor.getDoubleExtra("current_lat", 0.0);
            currentlng = postFavor.getDoubleExtra("current_lng", 0.0);


        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

     */
/*   MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);*//*


        tvBack = (ImageView) findViewById(R.id.tvBack);

        //ivDone = (ImageView) findViewById(R.id.ivDone);
        ivok = (Button) findViewById(R.id.ivOk);
        etStartLocation = (CustomAutoCompleteTextView) findViewById(R.id.etStartLocation);
        etStartLocation.setText("");
        llMain = (LinearLayout) findViewById(R.id.llMain);
        llMain.setOnClickListener(this);


        etStartLocation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etStartLocation.getRight() - etStartLocation.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                      */
/*  // your action here
                        if (closeEnable==true){
                            closeEnable=false;
                          //  etStartLocation.setCompoundDrawablesWithIntrinsicBounds( 0, 0,android.R.drawable.ic_menu_search,  0);
                            etStartLocation.setText("");

                        }else {
                            Utils.hideKeyBoard(GetAddressFromMapActivity.this, etStartLocation);
                            startLocation = etStartLocation.getText().toString();
                            new LATLONG().execute();
                        }*//*


                        if (!etStartLocation.getText().toString().trim().equalsIgnoreCase("")) {
                            etStartLocation.setText("");
                            etStartLocation.setFocusableInTouchMode(false);
                            etStartLocation.setFocusable(false);
                            etStartLocation.setFocusableInTouchMode(true);
                            etStartLocation.setFocusable(true);
                        }
                        return true;
                    }
                }

                return false;
            }
        });
        etStartLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                closeEnable = false;


                //etStartLocation.setCompoundDrawablesWithIntrinsicBounds( 0, 0,android.R.drawable.ic_menu_search,  0);
                locationSelected = "start";

                placeID = "";
                if (count > 0) {
                    // we don't want to make an insanely large array, so we
                    // clear it
                    // each time
                    // adapter.clear();
                    // create the task


                    if (fromMarker) {


                    } else {
                        placesTask = new PlacesTask();
                        placesTask.execute(s.toString());
                    }
                    fromMarker = false;
                    // System.out.println("Input = " + s.toString())}


                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        ivok.setOnClickListener(this);
        tvBack.setOnClickListener(this);
        //ivDone.setOnClickListener(this);

        address = currentAddress;
        etStartLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                HashMap<String, String> data = new HashMap<String, String>();
                data = al_data.get(position);
                data.toString();
                placeID = data.get("place_id");

                address = data.get("description");


                new AsyncTask<Void, Void, JSONObject>() {
                    @Override
                    protected JSONObject doInBackground(Void... params) {
                        return getLocationInfo(address);
                    }

                    @Override
                    protected void onPostExecute(JSONObject jsonObject) {
                        try {


                            currentLat = jsonObject.getJSONArray("results").getJSONObject(0).getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                            currentlng = jsonObject.getJSONArray("results").getJSONObject(0).getJSONObject("geometry").getJSONObject("location").getDouble("lng");

                            if (googleMap != null) {
                                googleMap.clear();
                                LatLng latLng = new LatLng(currentLat, currentlng);


                                Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng).title(address)
                                        .icon((BitmapDescriptorFactory.fromResource(R.drawable.ic_pin))));
                                marker.showInfoWindow();

                                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(14).build();
                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            } else {

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }.execute();


               */
/* Geocoder coder = new Geocoder(GetAddressFromMapActivity.this);
                try {
                    ArrayList<Address> adresses = (ArrayList<Address>) coder.getFromLocationName(data.get("description"), 1);
                    for (Address add : adresses) {

                        currentLat = add.getLatitude();
                        currentlng = add.getLongitude();

                        //endLat = mapLat;
                        // endLong = maplng;
                        if (googleMap != null) {
                            googleMap.clear();
                            LatLng latLng = new LatLng(currentLat, currentlng);


                            Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng).title(address)
                                    .icon((BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_green))));
                            marker.showInfoWindow();

                            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(14).build();
                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        } else {

                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }*//*



            }
        });


    }

    private void showOnMap(double latitude, double longitude) {
        if (googleMap != null) {

            googleMap.clear();
            LatLng latLng = new LatLng(latitude, longitude);


            Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng).title(address)
                    .icon((BitmapDescriptorFactory.fromResource(R.drawable.ic_pin))));
            marker.showInfoWindow();

            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(14).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


        }

    }

    // To Find Address City and Country Extra from LatLong.
    private void getLATLONGInfor(double lat, double lng) {
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        address = "";
        // String city = "";
        // String country = "";
        try {
            addresses = geocoder.getFromLocation(lat, lng, 1);

            Log.e("address line", "df" + addresses.get(0));
            Log.e("getMaxAddressLineIndex", "df" + addresses.get(0).getMaxAddressLineIndex());


            for (int b = 0; b <= addresses.get(0).getMaxAddressLineIndex(); b++) {
                address = address + addresses.get(0).getAddressLine(b) + " ";
            }

            //	if (addresses != null && addresses.size() > 0) {
            //	address = addresses.get(0).getAddressLine(0);
            // + addresses.get(0).getAddressLine(1)
            // + addresses.get(0).getAddressLine(2);
            // city = addresses.get(0).getLocality();
            // country = addresses.get(0).getCountryName();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    etStartLocation.setText(address);
                }
            });
            closeEnable = true;
            // etStartLocation.setCompoundDrawablesWithIntrinsicBounds( 0, 0,android.R.drawable.ic_menu_close_clear_cancel,  0);


            markerOptions.title(address);
            markerOptions.icon((BitmapDescriptorFactory.fromResource(R.drawable.ic_pin)));

            googleMap.clear();

            googleMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(lat, lng)));

            googleMap.addMarker(markerOptions);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showToast(String msg) {
        Utils.dialog(GetAddressFromMapActivity.this, msg);
    }

    @Override
    public void onClick(View v) {
        //   Utils.hideSoftKeyboard(GetAddressFromMapActivity.this);
        // Utils.hideKeyBoard(this, etStartLocation);

        if (!Utils.isInternetConnected(GetAddressFromMapActivity.this)) {
            showToast(getResources().getString(R.string.errorNoInternet));
            return;
        }
        switch (v.getId()) {
            case R.id.tvBack:

                finish();

                break;
            case R.id.ivOk:


                if (etStartLocation.getText().toString().trim().equals("")) {
                    showToast("Write location address");

                } else {

                    Intent result = new Intent();

                    result.putExtra("address", address);

                    result.putExtra("current_lat", currentLat);
                    result.putExtra("current_lng", currentlng);

                    // result.putExtra("current_address", currentAddress);
                    setResult(RESULT_OK, result);
                    finish();


                }


                break;

            default:
                break;
        }

    }

    @Override
    public void onMapReady(GoogleMap map) {


        googleMap = map;

        googleMap.setOnMapClickListener(this);

        if (address.trim().equals("")) {
            address = currentAddress;
            showOnMap(currentLat, currentlng);

        } else {
            address = address;
            if (address.trim().equalsIgnoreCase(currentAddress.trim())) {
                address = currentAddress;
                showOnMap(currentLat, currentlng);
            } else
                showOnMap(currentLat, currentlng);

        }
        etStartLocation.setText(address);

    }

    @Override
    public void onMapClick(LatLng latLng) {
        markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        currentlng = latLng.latitude;
        currentlng = latLng.longitude;
        fromMarker = true;
        getLATLONGInfor(latLng.latitude, latLng.longitude);


    }


    public static JSONObject getLocationInfo(String address) {
        StringBuilder stringBuilder = new StringBuilder();
        try {

            address = address.replaceAll(" ", "%20");

            HttpPost httppost = new HttpPost("http://maps.google.com/maps/api/geocode/json?address=" + address + "&sensor=false");
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            stringBuilder = new StringBuilder();


            response = client.execute(httppost);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    private class LATLONG extends AsyncTask<Void, Void, Void> {

        JSONObject jsonObject;
        private ProgressDialog progressDialog;

        //LoadToast lt =new LoadToast(ForgotPasswordActivity.this);

        @Override
        protected void onPreExecute() {

            super.onPreExecute();


            progressDialog = Utils.progressDialog(GetAddressFromMapActivity.this);
            progressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {


            try {


                end = getLocationInfo(address);

                if (getEndLatLong(end))
                    getLATLONGInfor(currentLat, currentlng);


            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void result) {

            super.onPostExecute(result);


            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            try {

                if (getEndLatLong(end))
                    showOnMap(currentLat, currentlng);
                else {
                    showToast("Google could not found location.");
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    public boolean getEndLatLong(JSONObject jsonObject) {

        try {


            currentlng = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                    .getJSONObject("geometry").getJSONObject("location")
                    .getDouble("lat");

            currentLat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                    .getJSONObject("geometry").getJSONObject("location")
                    .getDouble("lng");


        } catch (JSONException e) {


            // showToast("Google could not found location.");
            etStartLocation.setText(address);
            return false;

        }

        return true;
    }


    private class PlacesTask extends AsyncTask<String, Void, String> {

        // @Override
        // protected String doInBackground(String... place) {
        // // For storing data from web service
        // */
/*
        // * gpsTracker=new GPSTracker(AddEventActivity.this);
        // * if(gpsTracker.canGetLocation()) { lat=gpsTracker.getLatitude();
        // * lang=gpsTracker.getLongitude(); }
        // *//*

        // String data = "";
        //
        // // Obtain browser key from https://code.google.com/apis/console
        // String key = "key=AIzaSyCRgqJa_piMn2aRur_Udp33hnKNdZSUzl0";
        // googleKey = "AIzaSyCRgqJa_piMn2aRur_Udp33hnKNdZSUzl0";
        //
        // String input = "";
        //
        // try {
        // input = "input=" + URLEncoder.encode(place[0], "utf-8");
        // } catch (UnsupportedEncodingException e1) {
        // e1.printStackTrace();
        // }
        //
        // // place type to be searched
        // // String types = "types=geocode";
        //
        // // Sensor enabled
        // String sensor = "sensor=false";
        // // String
        // // location="location="+String.valueOf(lat)+","+String.valueOf(lang);
        // // Building the parameters to the web service
        // String parameters = input + "&" + sensor + "&" + key;
        //
        // // Output format
        // String output = "json";
        //
        // // Building the url to the web service
        // String url =
        // "https://maps.googleapis.com/maps/api/place/autocomplete/"
        // + output + "?" + parameters;
        // System.out.println("url = " + url);
        // try {
        // // Fetching the data from we service
        // data = downloadUrl(url);
        // } catch (Exception e) {
        // Log.d("Background Task", e.toString());
        // }
        // return data;
        // }

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            */
/*
             * gpsTracker=new GPSTracker(AddEventActivity.this);
			 * if(gpsTracker.canGetLocation()) { lat=gpsTracker.getLatitude();
			 * lang=gpsTracker.getLongitude(); }
			 *//*

            String data = "";

            // Obtain browser key from https://code.google.com/apis/console
            String key = "key=AIzaSyCJGMVkbSwBm2Tl0bJFnPNSak5zzHJ_H_w";
            googleKey = "AIzaSyCJGMVkbSwBm2Tl0bJFnPNSak5zzHJ_H_w";

            String input = "";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            // place type to be searched
            String types = "types=geocode";

            // Sensor enabled
            String sensor = "sensor=true";
            // String
            // location="location="+String.valueOf(lat)+","+String.valueOf(lang);
            // Building the parameters to the web service
*/
/*
            String parameters = input + "&" + sensor + "&" + key + "&"
                    + "location=0.000000,0.000000" + "&" + "radius=100.000000";
*//*

            String parameters = input + "&" + sensor + "&" + key;

            // Output format
            String output = "json";

            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/"
                    + output + "?" + parameters;
            // System.out.println("url = " + url);
            try {
                // Fetching the data from we service
                data = downloadUrl(url);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Creating ParserTask
            parserAutoTask = new ParserAutoTask();

            // Starting Parsing the JSON string returned by Web Service
            parserAutoTask.execute(result);
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception in url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }

        return data;

    }

    private class ParserAutoTask extends
            AsyncTask<String, Integer, List<HashMap<String, String>>> {

        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(
                String... jsonData) {

            List<HashMap<String, String>> places = null;

            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);

                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {

            // JSONObject


            //
            // // Creating a SimpleAdapter for the AutoCompleteTextView
            // //changed
            // // al_data to result
            // SimpleAdapter adapter = new SimpleAdapter(getBaseContext(),
            // al_data, android.R.layout.simple_list_item_1, from, to);
            //
            // // Setting the adapter
            // etAddress.setAdapter(adapter);
            // adapter.notifyDataSetChanged();

            //  int[] to = new int[] { R.id.TextAddressResult };

            // Creating a SimpleAdapter for the AutoCompleteTextView //changed
            // al_data to result
            SimpleAdapter adapter;
            try {
                data = new JSONArray(result);

                al_data = new ArrayList<>();
                al_data.addAll(result);

                String[] from = new String[]{"description"};
                int[] to = new int[]{android.R.id.text1};
                adapter = new SimpleAdapter(GetAddressFromMapActivity.this,
                        al_data, android.R.layout.simple_list_item_1, from, to);

                // Setting the adapter
                if (locationSelected.equals("start")) {
                    etStartLocation.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {

                    // TODO for destination
                    //  etDestLocation.setAdapter(adapter);
                    //  adapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }


    }
}
*/
