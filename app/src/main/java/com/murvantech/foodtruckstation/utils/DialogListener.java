package com.murvantech.foodtruckstation.utils;

/**
 * Created by logicspice on 19/6/17.
 */

public interface DialogListener {
    public void onClick();
}
